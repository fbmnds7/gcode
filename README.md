# cnc-host
### _Your Name <your.name@example.com>_

This is a project to do ... something.

## Stage 1 - Pre-flight check of a Gcode block

- use previous gc-state-0 or initialize gc-state-0
- discard lower and upper control char ranges silently
- GC-NIL on empty block
  - gc-state-0 is previous state for subsequent block
- GC-COMMENT on comment block, also if numbered
  - gc-state-0 is previous state for subsequent block

## Stage 2 - Tokenize a Gcode block

- remove inline comments
- trim whitespace
- tokenize Gcode block
  - uppercased words
  - float parameter words grouped with values
  - discard not implemented words silently
    - formal check on first letter of word
  - discard line numbering
  - preserve word sequence
- GC-ERROR on missing float values
  - gc-state-0 is previous state for error handling

## Stage 3 - Break up a Gcode block

- split word word list in three parts
  - pivot on first implemented motion word G0-3, G17-19
  - until, excluding a M word
  - from excluded M word
- for each part
  - separate float parameter words from other words 
  - check modal constraints for G words
  - check modal constraints for M words
- GC-ERROR on any modal constraint violation
  - gc-state-0 is previous state for error handling

## Stage 4 - Analyze pre-command

- TODO update spindle state
- TODO update feed state
- convert inch to mm in pre-command
  - gc-state-0 unit is :inch
  - staged pre-command list contains :G20
- prune words, if effectless on gc-state-0
  - keep jog/linear motion word
  - add jog/linear motion word according to gc-state-0, if missing in block
- GC-NIL
  - empty pre-commands list
  - empty float parameter object, i.e. all slots have value nil
  - gc-pre-state is previous state for subsequent stage
- GC-LINEAR-MOTION 
  - jog/linear motion word derived from gc-state-0
    - holds implicitely because of stage 3
    - jog/linear motion word added from gc-state-0 
  - no incremental float parameters, ref. woc-inc-slots
  - update gc-pre-state
    - update woc position
    - assume default unit mm
  - previous state for subsequent block is undefined
  - gc-pre-state is previous state for subsequent stage
- GC-MULTI-CMD 
  - no jog/linear motion word
  - no motion float parameters x, y, z
  - no incremental float parameters, ref. woc-inc-slots
  - update gc-pre-state
  - gc-pre-state is previous state for subsequent stage
  - previous state for subsequent block is undefined
- GC-ERROR on incomplete linear motion pre-command
  - motion float parameters are present
  - no jog/linear motion word derived from gc-state-0
  - gc-state-0 is previous state for error handling
 
## Stage 5 - Analyze main command

- TODO update spindle state
- TODO update feed state
- convert inch to mm in main-command
  - gc-pre-state unit is :inch
  - staged main-command list contains :G20
- prune words, if effectless on gc-pre-state
  - keep jog/linear motion word
  - add jog/linear motion word according to gc-main-state, if missing in block
- GC-NIL
  - empty main commands list
  - empty float parameter object, i.e. all slots have value nil
  - gc-main-state is previous state for subsequent stage
- GC-LINEAR-MOTION 
  - jog/linear motion word present in main commands list
  - jog/linear motion word derived from gc-pre-state
    - jog/linear motion word added from gc-pre-state 
    - no incremental float parameters, ref. woc-inc-slots
  - update gc-main-state
    - update woc position
    - assume default unit mm
  - previous state for subsequent block is undefined
  - gc-main-state is previous state for subsequent stage
- GC-CIRCLE, GC-HELIX 
  - cw/ccw motion word present in main commands list
  - cw/ccw motion word derived from gc-pre-state
    - cw/ccw motion word added from gc-pre-state
    - plane specifier word added 
  - incremental float parameters according to type and plane
  - update gc-main-state
    - update woc position
    - assume default unit mm
  - previous state for subsequent block is undefined
  - gc-main-state is previous state for subsequent stage
- GC-ERROR on GC-MULTI-CMD 
  - invalid for stage 5
  - gc-pre-state is previous state for error handling
  - previous state for subsequent block is undefined
    - TODO propagate gc-pre-state to gc-main-state?
- GC-NIL
  - TODO is pre-cmd/post-cmd/no main-cmd a reasonable use case? 
  - empty staged-main-cmds
  - propagate gc-pre-state to gc-main-state
  - gc-main-state is previous state for subsequent stage
  - previous state for subsequent block is undefined
- GC-ERROR on incomplete linear motion main-command
  - motion float parameters are present
  - no jog/linear motion word derived from gc-pre-state
  - gc-pre-state is previous state for error handling
    - TODO propagate gc-pre-state to gc-main-state?
- GC-ERROR on any other staged main commands 
  - gc-main-state is previous state for error handling
    - TODO propagate gc-main-state to gc-post-state?

## Stage 6 - Analyze post command

- TODO update spindle state
- TODO update feed state
- convert inch to mm in post-command
  - gc-main-state unit is :inch
  - staged post-command list contains :G20
- prune words, if effectless on gc-main-state
  - keep jog/linear motion word
  - add jog/linear motion word according to gc-main-state, if missing in block
- GC-NIL
  - empty main commands list
  - empty float parameter object, i.e. all slots have value nil
  - gc-main-state is previous state for subsequent stage
- GC-LINEAR-MOTION 
  - jog/linear motion word present in post commands list
  - jog/linear motion word derived from gc-main-state
    - jog/linear motion word added from gc-main-state 
    - no incremental float parameters, ref. woc-inc-slots
  - update gc-post-state
    - update woc position
    - assume default unit mm
- GC-MULTI-CMD 
  - no jog/linear motion word
  - no motion float parameters x, y, z
  - no incremental float parameters, ref. woc-inc-slots
  - update gc-post-state
    - update woc position
    - assume default unit mm
- GC-NIL
  - empty staged-post-cmds
  - propagate gc-main-state to gc-post-state
- GC-ERROR on any other staged post commands

## License

Specify license here

