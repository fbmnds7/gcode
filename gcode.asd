;;;; cnc-host.asd

(asdf:defsystem #:gcode
  :description "Describe gcode here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on
  (#:asdf
   #:quicklisp
   #:str
   #:lisp-unit
   #:misc
   #:cl-ppcre)
  :serial t
  :components
  ((:file "package")
   (:module "src"
    :components
    ((:file "data-utils")
     (:file "gcode-const")
     (:file "gcode-data")
     (:file "gcode")))))

