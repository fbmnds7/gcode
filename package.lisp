

#+ccl (rename-package 'lisp-unit 'lisp-unit '(lu))

(defpackage #:gcode
  (:use #:cl)
  #-ccl(:local-nicknames (#:§ #:misc)
                         (#:ppcre #:cl-ppcre)
                         (#:str #:str)
                         (#:lu #:lisp-unit))
  (:export #:gcode
           #:clone-by-slots
           #:copy-by-slots
           #:emit-gcode
           #:f
           #:format-by-slots
           #:format-float-params
           #:format-gcode
           #:format-gc-error
           #:f-params
           #:gc-block
           #:gc-cmd
           #:gc-list
           #:gc-state-0
           #:gc-main-state
           #:gc-post-state
           #:gc-pre-state
           #:gc-state
           #:gc-state-slots
           #:gc-text
           #:gc-type
           #:i
           #:j
           #:k
           #:main-cmd
           #:make-stage-acc
           #:mode
           #:motion
           #:point-3
           #:point-3=
           #:post-cmd
           #:pre-cmd
           #:r
           #:s
           #:stage-1
           #:stage-2
           #:stage-3
           #:stage-4
           #:stage-5
           #:stage-6
           #:stage-7
           #:stage-acc
           #:stage-acc-slots
           #:unit
           #:woc-pos
           #:x
           #:y
           #:z))



