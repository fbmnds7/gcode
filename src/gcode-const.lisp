(in-package #:gcode)

(defvar **const** nil)

(unless **const**
  (defconstant g-cmds '("G00G41" "G01G41" "G00G42" "G01G42"
                        "G28.1"
                        "G0G41" "G1G41" "G0G42" "G1G42"
                        "G54.1"
                        "G84.2" "G84.3" 
                        "G90.1" "G91.1" "G92.1" "G92.2" "G92.3"
                        "G00" "G01" "G02" "G03" "G04" "G09"
                        "G10" "G12" "G13" "G15" "G16" "G17" "G18" "G19"
                        "G20" "G21" "G28"
                        "G30" "G31" "G32"
                        "G40" "G41" "G42" "G43" "G44" "G49"
                        "G50" "G51" "G52" "G53" "G54" "G55" "G56" "G57" "G58" "G59"
                        "G60" "G61" "G64" "G65" "G66" "G68" "G69"
                        "G70" "G71" "G73" "G74" "G76"
                        "G80" "G81" "G82" "G83" "G84" "G85" "G86" "G87" "G88" "G89"
                        "G90" "G91" "G92" "G93" "G94" "G95" "G96" "G97" "G98" "G99"
                        "G0"  "G1"  "G2"  "G3"  "G4"  "G9"))

  (defconstant m-cmds '("M200" "M201" "M202" "M203" "M204" "M206" "M206" "M207"
                        "M208" "M209" "M210" "M211" "M212" "M213" "M214" "M215"
                        "M216" "M217" "M218" "M219" "M220" "M221"
                        "M228"
                        "M00" "M01" "M02" "M03" "M04" "M05" "M06" "M07" "M08" "M09"
                        "M19"
                        "M30"
                        "M40" "M41" "M42" "M43" "M44" "M45" "M47" "M48" "M49"
                        "M98" "M99" 
                        "M0"  "M1"  "M2"  "M3"  "M4"  "M5"  "M6"  "M7"  "M8"  "M9"))
  
  (defconstant modal-group-0 '(:G04 :G10   :G28   :G30   :G53
                               :G92 :G92.1 :G92.2 :G92.3))

  (defconstant modal-group-1 '(:G38.2
                               :G00 :G01 :G02 :G03 
                               :G80 :G81 :G82 :G84 :G85 :G86 :G87 :G88 :G89))

  (defconstant modal-group-2 '(:G17 :G18 :G19))

  (defconstant modal-group-3 '(:G90 :G91))

  (defconstant modal-group-4-m '(:M00 :M01 :M02 :M03 :M30))

  (defconstant modal-group-5 '(:G93 :G94))

  (defconstant modal-group-6 '(:G20 :G21))

  (defconstant modal-group-6-m '(:M06))

  (defconstant modal-group-7 '(:G40 :G41 :G42))

  (defconstant modal-group-7-m '(:M03 :M04 :M05))

  (defconstant modal-group-8 '(:G43 :G49))

  (defconstant modal-group-8-m '(:M07 :M08 :M09))

  (defconstant modal-group-9-m '(:M48 :M49))

  (defconstant modal-group-10 '(:G98 :G99))

  (defconstant modal-group-12 '(:G54 :G55 :G56 :G57 :G58 :G59))

  (defconstant modal-group-13 '(:G61 :G61.1 :G64))

  (defconstant valid-token-letter '(#\G #\X #\Y #\Z #\A #\B #\C #\I #\J #\K #\R
                                    #\M #\S #\T #\H #\D #\F #\P #\Q #\L #\N))

  (defconstant short-cmds-letter '(#\G #\M #\T #\D #\L))

  (defconstant implemented-token '(#\G #\X #\Y #\Z #\I #\J #\K #\M #\S #\F))

  (defconstant implemented-float-token '(#\X #\Y #\Z #\I #\J #\K #\S #\F))

  (defconstant implemented-cmds '("G00" "G01" "G02" "G03" "G20" "G21" "G90" "G91"
                                  "G0"  "G1"  "G2"  "G3"
                                  "G17" "G18" "G19"
                                  "M00" "M01" "M02" "M03" "M04" "M05"
                                  "M06" "M07" "M08" "M09"
                                  "M30"
                                  "M0"  "M1"  "M2"  "M3"  "M4"  "M5"
                                  "M6"  "M7"  "M8"  "M9"))

  (defconstant implemented-motion-cmds '("G00" "G01" "G02" "G03"
                                         "G0"  "G1"  "G2"  "G3"
                                         "G17" "G18" "G19"))

  (defconstant gc-motion-kw '(:g00 :g01 :g02 :g03))

  (defconstant gc-cmd-motion-slots '(gc-type gc-list f-params))

  (defconstant float-params-slots '(x y z i j k r s f))

  (defconstant convertible-slots '(x y z i j k r))

  (defconstant woc-inc-slots '(i j k r))

  (defconstant feed-state-slots '(f-g0 $f-g0 $f-g0-min $f-g0-max
                                  f-g1 $f-g1 $f-g1-min $f-g1-max))

  (defconstant spindle-state-slots '(s        ; (float)
                                     spin     ; :cw :ccw  
                                     flood    ; :on 
                                     mist))   ; :on

  (defconstant point-3-slots '(x y z))

  (defconstant gc-state-base-slots '(unit motor motion mode))

  (defconstant gc-state-slots '(unit       ; ;inch(=G20) :mm(=G21)
                                motor      ; :on 
                                motion     ; :jog(=G0) :lin(=G1) :cw(=G2) :ccw(=G3)
                                mode       ; :abs(=G90) :inc(=G91)
                                moc-offset ; point-3
                                woc-pos    ; point-3
                                feed
                                spindle))

  (defconstant staged-cmds-slots '(cmds fps))

  (defconstant stage-acc-slots '(final
                                 gc-block
                                 gc-state-0
                                 gc-pre-state
                                 gc-main-state
                                 gc-post-state
                                 staged-pre-cmds
                                 staged-main-cmds
                                 staged-post-cmds
                                 pre-cmd
                                 main-cmd
                                 post-cmd)))

(setf **const** t)


