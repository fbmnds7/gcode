(in-package #:gcode)


(defun slot (f x) `(,f ,x))

(defun set-slot (f x v) `(when ',f (setf (,f ,x) ,v)))

(defun slot->defclass-slot (slot)
  `(,slot :initarg ,(§:make-upcase-keyword slot)
	  :initform nil
	  :accessor ,slot))

(defmacro defclass-by-slots (name supers slots-symbol)
  "The value of a symbol of slots-symbol is used as the
  list of slots."
  `(defclass ,name ,supers
     ,(if (and (symbolp slots-symbol)
               (symbol-value slots-symbol)
               (listp (symbol-value slots-symbol)))
          (mapcar #'slot->defclass-slot (symbol-value slots-symbol))
          (error "~a is not a symbol which names a list of slot names" slots-symbol))))

(defun copy-slot (s d slot)
  `(setf (,slot ,d) (,slot ,s)))

(defun copy-by-slots (s d slots)
  (assert (eql (class-of s) (class-of d)))
  (let ((f (lambda (s$) (eval (copy-slot s d s$)))))
    (loop for s in slots do (funcall f s)))
  d)

(defun clone-by-slots (o slots)
  (let ((oo (make-instance (class-of o))))
    (copy-by-slots o oo slots)
    oo))

(defun format-slot (o slot)
  `(if (slot-exists-p ,o ',slot)
       (format nil "~%~A : ~A" ',slot (,slot ,o))
       nil))

(defun format-replace (s str-type)
  (ppcre:regex-replace-all
   #+sbcl (format nil "#<~A \\{(\\d|[A-F])+\\}>" str-type)
   #+ccl (format nil "#<~A #x(\\d|[A-F])+>" str-type)
   s
   (format nil "#<~A {...}>" str-type)))

#+ecl
(defun format-replace (s str-type)
  (ppcre:regex-replace-all
   (format nil "#<a .*::~A 0x(\\d|[a-f])+> \\(#<The " str-type)
   s
   (format nil "#<~A {...}> (#<" str-type)))

(defun format-by-slots (o slots &optional str-type)
  (let* ((f (lambda (s$) (eval (format-slot o s$))))
         (str-type (string-upcase str-type))
         (r (concatenate
             'string
             (format nil "~A (~A)" o (class-of o))
             (reduce (lambda (s1 s2) (concatenate 'string s1 s2))
                     (loop for s in slots
                           when (funcall f s) collect it)
                     :from-end t :initial-value (format nil "~%")))))
    (if str-type (format-replace r str-type) r)))

(defun eql-slot (s d slot)
  `(eql (,slot ,d) (,slot ,s)))

(defun eql-by-slots (s d slots)
  (assert (eql (class-of s) (class-of d)))
  (let ((f (lambda (s$) (eval (eql-slot s d s$)))))
    (every #'identity (loop for s in slots collect (funcall f s)))))

(defun equal-slot (s d slot)
  `(equal (,slot ,d) (,slot ,s)))

(defun equal-by-slots (s d slots)
  (assert (eql (class-of s) (class-of d)))
  (let ((f (lambda (s$) (eval (equal-slot s d s$)))))
    (every #'identity (loop for s in slots collect (funcall f s)))))

(defun slot= (s d slot)
  `(cond ((and (null (,slot ,s)) (null (,slot ,d))))
         ((or (null (,slot ,s)) (null (,slot ,d))) nil)
         (t (= (,slot ,d) (,slot ,s)))))

(defun slots= (s d slots)
  (cond ((eql (class-of s) (class-of d))
         (let ((f (lambda (s$) (eval (slot= s d s$)))))
           (every #'identity (loop for s in slots collect (funcall f s)))))
        ((and (endp s) (endp d)))
        (t nil)))
