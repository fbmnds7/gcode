(defparameter *f* "~/projects/gcode/src/gcode.lisp")

(defun pick-doc (nc s)
  (when (and (eql 'defmethod (first nc))
             (eql s (second nc)))
    (format nil "~a~%~%" (nth 3 nc))))

(defun readme (f)
  (with-open-file (in f)
    (loop for nc = (unwind-protect (read in nil nil nil) nil)
          with d = (or (pick-doc nc 'stage-1)
                       (pick-doc nc 'stage-2)
                       (pick-doc nc 'stage-3)
                       (pick-doc nc 'stage-4)
                       (pick-doc nc 'stage-5)
                       (pick-doc nc 'stage-6)
                       (pick-doc nc 'stage-7))
          when d collect it)))



;;(format nil "~a~%~a~%~a~%~a~%~a~%~a~%~a~%" )
