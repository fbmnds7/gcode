(in-package #:gcode)

;;(defclass-by-slots params (x y z i j k r f s p q o))

(defclass-by-slots float-params nil float-params-slots)
;; (mapcar #'(lambda (c) (make-symbol (string c))) valid-token-letter-float)

(defmethod format-float-params ((fps list) &key (emit nil))
  (declare (ignore emit))
  (format nil "#<FLOAT-PARAMS {...}> : ~A" fps))

(defmethod format-float-params ((fps float-params) &key (emit nil))
  (if emit
      (str:unwords
       (mapcar (lambda (x) (format nil "~a ~,3f" (first x) (second x)))
               (remove-if #'null (mapcar (lambda (p)
                                           (eval `(if (,p ,fps)
                                                      (list ',p (,p ,fps))
                                                      nil)))
                                         float-params-slots))))
      (format-by-slots fps float-params-slots "FLOAT-PARAMS")))

(defmethod float-params= ((f1 float-params) (f2 float-params))
  (slots= f1 f2 float-params-slots))

(defmethod float-params= ((f1 list) (f2 float-params))
  (lu:assert-nil f1)
  nil)

(defmethod float-params= ((f1 float-params) (f2 list))
  (lu:assert-nil f2)
  nil)

(defmethod float-params= ((f1 list) (f2 list))
  (lu:assert-nil f1)
  (lu:assert-nil f2)
  t)

(defclass gcode ()
  ((gc-type
    :initarg :gc-type
    :accessor gc-type)
   (gc-text
    :initarg :gc-text
    :initform nil
    :accessor gc-text)
   (gc-format
    :initform (lambda (gc) (format nil "(~A : ~A)" gc (gc-text gc)))
    :accessor gc-format)
   (gc-emit
    :initform (lambda (gc) (format nil "(~A : ~A)" gc (gc-text gc)))
    :accessor gc-emit)))

(defmethod emit-gcode ((gc gcode)) (funcall (gc-emit gc) gc))

(defmethod format-gcode ((gc gcode))
  (funcall (gc-format gc) gc))

(defun make-gcode (gc-type &optional (other nil))
  (cond ((eql gc-type :gc-nil)
         (make-instance 'gc-nil))
        ((or (eql gc-type :gc-comment)
             (eql gc-type :gc-error))
	 (if (typep other 'string)
	     (make-instance (if (eql gc-type :gc-comment)
                                'gc-comment
                                'gc-error)
                            :gc-text other)
	     (error "invalid gcode comment/error")))
	((typep other 'float-params)
	 (make-instance 'gcode :gc-type gc-type :f-params other))
	(t
	 (make-instance 'gcode))))

(defclass gc-nil (gcode)
  ((gc-type
    :initarg :gc-type
    :initform :gc-nil
    :reader gc-type)))

(defclass gc-comment (gcode)
  ((gc-type
    :initarg :gc-type
    :initform :gc-comment
    :reader gc-type)))

(defun make-gc-comment (text)
  (make-instance 'gc-comment :gc-text text))

(defclass gc-error (gcode)
  ((gc-type
    :initform :gc-error
    :reader gc-type)))

(defun make-gc-error (text)
  (make-instance 'gc-error :gc-text text))

(defmethod format-gc-error ((gc-err gc-error))
  (format-by-slots gc-err '(gc-type gc-text)))

(defgeneric format-gc-cmd (gc-cmd))

(defclass gc-cmd (gcode)
  ((gc-type
    :initarg :gc-type
    :accessor gc-type)
   (gc-list
    :initarg :gc-list
    :accessor gc-list)
   (f-params
    :initarg :f-params
    :accessor f-params)
   (gc-format
    :initform (lambda (gc)
                (format nil "(~A : ~A ~A ~A)"
                        (gc-type gc)
                        (gc-text gc)
                        (gc-list gc)
                        (str:replace-all (string #\Newline) " "
                                         (format-float-params (f-params gc)))))
    :reader gc-format)
   (gc-emit
    :initform (lambda (gc)
                (format nil "~a ~a"
                        (string-trim '(#\( #\)) (format nil "~a" (gc-list gc)))
                        (format-float-params (f-params gc) :emit t)))
    :reader gc-emit)))

(defmethod format-gc-cmd ((gc gc-cmd))
  (format nil "~A  ~A"
          (mapcar #'string (gc-list gc))
          (concatenate 'string
                       (loop for i in float-params-slots
                             with fp = (funcall (symbol-value i) (f-params gc))
                             when fp collect (format nil "~A~2$ " i fp)))))

(defclass gc-multi-cmd (gc-cmd)
  ((gc-type
    :initform :gc-multi-cmd
    :reader gc-type)))

(defun make-gc-multi-cmd (cmds fps)
  (make-instance 'gc-multi-cmd :gc-list cmds
                               :f-params fps))

(defclass gc-circle (gc-cmd)
  ((f-params
    :initarg :f-params
    :accessor f-params)))

(defun make-gc-circle (gc-list f-params)
  (assert (or (member :G02 gc-list)
              (member :G03 gc-list)))
  (make-instance 'gc-circle
                 :gc-type :gc-circle
                 :gc-list gc-list
                 :f-params f-params))

(defclass gc-helix (gc-cmd)
  ((f-params
    :initarg :f-params
    :accessor f-params)))

(defun make-gc-helix (gc-list f-params)
  (assert (or (member :G02 gc-list)
              (member :G03 gc-list)))
  (make-instance 'gc-helix
                 :gc-type :gc-helix
                 :gc-list gc-list
                 :f-params f-params))

(defclass gc-motion (gc-cmd)
  ((f-params
    :initarg :f-params
    :accessor f-params)))

(defmethod make-gc-motion ((st-cmds list) (st-fps float-params))
  (make-instance 'gc-motion
                 :gc-type :gc-motion
                 :gc-list st-cmds
                 :f-params st-fps))

(defmethod make-gc-motion ((st-cmds list) (st-fps list))
  (assert (eql st-fps nil))
  (make-instance 'gc-motion
                 :gc-type :gc-motion
                 :gc-list st-cmds
                 :f-params (make-instance 'float-params)))

(defun gc-cmd= (gc-1 gc-2)
  (cond ((and (null gc-1) (null gc-2)))
        ((or (null gc-1) (null gc-2)) nil)
        ((not (equal (type-of gc-1) (type-of gc-2))) nil)
        ((not (equal (gc-type gc-1) (gc-type gc-2))) nil)
        ((equal :gc-nil (gc-type gc-1)))
        ((and (member (gc-type gc-1)
                      '(:gc-error :gc-comment :gc-text)
                      :test #'equal)
              (equal (gc-text gc-1) (gc-text gc-2))))
        ((and (typep gc-1 'gc-cmd)
              (typep gc-2 'gc-cmd)
              (equal (gc-list gc-1) (gc-list gc-2))
              (float-params= (f-params gc-1) (f-params gc-2))))
        (t nil)))

(defclass-by-slots woc-inc nil woc-inc-slots)

(defmethod format-woc-inc ((w-inc list))
  (format nil "#<WOC-INC {...}> : ~A" w-inc))

(defmethod format-woc-inc ((w-inc woc-inc))
  (format-by-slots w-inc woc-inc-slots "WOC-INC"))

(defmethod woc-inc= ((w-inc-1 list) (w-inc-2 list))
  (lu:assert-nil w-inc-1)
  (lu:assert-nil w-inc-2)
  t)

(defmethod woc-inc= ((w-inc-1 list) (w-inc-2 woc-inc))
  (lu:assert-nil w-inc-1)
  nil)

(defmethod woc-inc= ((w-inc-1 woc-inc) (w-inc-2 list))
  (lu:assert-nil w-inc-2)
  nil)

(defmethod woc-inc= ((w-inc-1 woc-inc) (w-inc-2 woc-inc))
  (slots= w-inc-1 w-inc-2 woc-inc-slots))

(defclass-by-slots feed-state nil feed-state-slots)

(defmethod format-feed-state ((f-st list))
  (format nil "#<FEED-STATE {...}> : ~A" f-st))

(defmethod format-feed-state ((f-st feed-state))
  (format-by-slots f-st feed-state-slots "FEED-STATE"))

(defmethod feed-state= ((f-st-1 list) (f-st-2 list))
  (lu:assert-nil f-st-1)
  (lu:assert-nil f-st-2)
  t)

(defmethod feed-state= ((f-st-1 list) (f-st-2 feed-state))
  (lu:assert-nil f-st-1)
  nil)

(defmethod feed-state= ((f-st-1 feed-state) (f-st-2 list))
  (lu:assert-nil f-st-2)
  nil)

(defmethod feed-state= ((f-st-1 feed-state) (f-st-2 feed-state))
  (slots= f-st-1 f-st-2 feed-state-slots))

(defclass-by-slots spindle-state nil spindle-state-slots)

(defmethod format-spindle-state ((sp-st list))
  (format nil "#<SPINDLE-STATE {...}> : ~A" sp-st))

(defmethod format-spindle-state ((sp-st spindle-state))
  (format-by-slots sp-st spindle-state-slots "SPINDLE-STATE"))

(defmethod spindle-state= ((sp-st-1 list) (sp-st-2 list))
  (lu:assert-nil sp-st-1)
  (lu:assert-nil sp-st-2)
  t)

(defmethod spindle-state= ((sp-st-1 list) (sp-st-2 spindle-state))
  (lu:assert-nil sp-st-1)
  nil)

(defmethod spindle-state= ((sp-st-1 spindle-state) (sp-st-2 list))
  (lu:assert-nil sp-st-2)
  nil)

(defmethod spindle-state= ((sp-st-1 spindle-state) (sp-st-2 spindle-state))
  (lu:assert-equal '(s spin flood mist) spindle-state-slots)
  (and (slots= sp-st-1 sp-st-2 '(s))
       (equal-by-slots sp-st-1 sp-st-2 '(spin flood mist))))

(defclass-by-slots point-3 nil point-3-slots)

(defmethod format-point-3 ((p list))
  (format nil "#<POINT-3 {...}> : ~A" p))

(defmethod format-point-3 ((p point-3))
  (format-by-slots p point-3-slots "POINT-3"))

(defmethod point-3= ((p3-1 list) (p3-2 list))
  (lu:assert-nil p3-1)
  (lu:assert-nil p3-2)
  t)

(defmethod point-3= ((p3-1 list) (p3-2 point-3))
  (lu:assert-nil p3-1)
  nil)

(defmethod point-3= ((p3-1 point-3) (p3-2 list))
  (lu:assert-nil p3-2)
  nil)

(defmethod point-3= ((p3-1 point-3) (p3-2 point-3))
  (slots= p3-1 p3-2 point-3-slots))

(defclass-by-slots gc-state nil gc-state-slots)

(defmethod format-gc-state ((gc-st list))
  (format nil "#<GC-STATE {...}> : ~A~%" gc-st))

(defmethod format-gc-state ((gc-st gc-state))
  (let ((f-gc-state (format-by-slots gc-st gc-state-base-slots "GC-STATE")))
    (format nil "~A~A~A~A~A"
            f-gc-state
            (format nil "MOC-OFFSET : ~A~%" (format-point-3 (moc-offset gc-st)))
            (format nil "WOC-POS : ~A~%" (format-point-3 (woc-pos gc-st)))
            (format nil "FEED : ~A~%" (format-feed-state (feed gc-st))) 
            (format nil "SPINDLE : ~A~%" (format-spindle-state (spindle gc-st))))))

(defmethod gc-state= ((gc-st-1 list) (gc-st-2 list))
  (lu:assert-nil gc-st-1)
  (lu:assert-nil gc-st-2)
  t)

(defmethod gc-state= ((gc-st-1 list) (gc-st-2 gc-state))
  (lu:assert-nil gc-st-1)
  nil)

(defmethod gc-state= ((gc-st-1 gc-state) (gc-st-2 list))
  (lu:assert-nil gc-st-2)
  nil)

(defmethod gc-state= ((gc-st-1 gc-state) (gc-st-2 gc-state))
  (and (equal-by-slots gc-st-1 gc-st-2 gc-state-base-slots)
       (point-3= (moc-offset gc-st-1) (moc-offset gc-st-2))
       (point-3= (woc-pos gc-st-1) (woc-pos gc-st-2))
       (feed-state= (feed gc-st-1) (feed gc-st-2))
       (spindle-state= (spindle gc-st-1) (spindle gc-st-2))))

(defclass-by-slots staged-cmds nil staged-cmds-slots)

(defmethod format-staged-cmds ((st-cmds list))
  (format nil "#<STAGED-CMDS {...}> : ~A~%" st-cmds))

(defmethod format-staged-cmds ((st-cmds staged-cmds))
  (lu:assert-equal '(cmds fps) staged-cmds-slots)
  (let ((f-st-cmds (format-by-slots st-cmds '(cmds) "STAGED-CMDS")))
    (format nil "~A~A" f-st-cmds (format-float-params (fps st-cmds)))))

(defun make-staged-cmds (cmds fps)
  (make-instance 'staged-cmds :cmds cmds :fps fps))

(defmethod staged-cmds= ((st-cmds-1 list) (st-cmds-2 list))
  (lu:assert-nil st-cmds-1)
  (lu:assert-nil st-cmds-2)
  t)

(defmethod staged-cmds= ((st-cmds-1 list) (st-cmds-2 staged-cmds))
  (lu:assert-nil st-cmds-1)
  nil)

(defmethod staged-cmds= ((st-cmds-1 staged-cmds) (st-cmds-2 list))
  (lu:assert-nil st-cmds-2)
  nil)

(defmethod staged-cmds= ((st-cmds-1 staged-cmds) (st-cmds-2 staged-cmds))
  (lu:assert-equal '(cmds fps) staged-cmds-slots)
  (and (equal (cmds st-cmds-1) (cmds st-cmds-2))
       (slots= (fps st-cmds-1) (fps st-cmds-2) float-params-slots)))

#|
(defclass maybe-staged-cmds (maybe_t)
  ((monad :initform (make-instance 'staged-cmds))))

(defmethod cmds ((m maybe-staged-cmds)) (pure (monad-fn m 'cmds)))

(defmethod fps ((m maybe-staged-cmds)) (pure (monad-fn m 'fps)))
|#

(defclass-by-slots stage-acc nil stage-acc-slots)

(defmethod make-stage-acc ((s string))
  (make-instance 'stage-acc :gc-block s))

(defmethod format-stage-acc ((st-acc list))
  (format nil "#<STAGE-ACC {...}> : ~A~%" st-acc))

(defmethod format-stage-acc ((st-acc stage-acc))
  (lu:assert-equal '(final gc-block
                  gc-state-0 gc-pre-state gc-main-state gc-post-state
                  staged-pre-cmds staged-main-cmds staged-post-cmds
                  pre-cmd main-cmd post-cmd)
                stage-acc-slots)
  (let ((f-st-acc (format-by-slots st-acc '(final gc-block)
                                   "STAGE-ACC")))
    (format nil "~A~A~A~A~A~A~A~A~A~A~A"
            f-st-acc
            (format nil "GC-STATE-0 : ~A" (format-gc-state (gc-state-0 st-acc)))
            (format nil "GC-PRE-STATE : ~A" (format-gc-state
                                             (gc-pre-state st-acc)))
            (format nil "GC-MAIN-STATE : ~A" (format-gc-state
                                              (gc-main-state st-acc)))
            (format nil "GC-POST-STATE : ~A" (format-gc-state
                                              (gc-post-state st-acc)))
            (format nil "STAGED-PRE-CMDS : ~A" (format-staged-cmds
                                                (staged-pre-cmds st-acc)))
            (format nil "STAGED-MAIN-CMDS : ~A" (format-staged-cmds
                                                 (staged-main-cmds st-acc)))
            (format nil "STAGED-POST-CMDS : ~A" (format-staged-cmds
                                                 (staged-post-cmds st-acc)))
            (format nil "PRE-CMD : ~A" (pre-cmd st-acc))
            (format nil "MAIN-CMD : ~A" (main-cmd st-acc))
            (format nil "POST-CMD : ~A" (post-cmd st-acc)))))

(defmethod stage-acc= ((st-acc-1 list) (st-acc-2 list))
  (lu:assert-nil st-acc-1)
  (lu:assert-nil st-acc-2)
  t)

(defmethod stage-acc= ((st-acc-1 list) (st-acc-2 stage-acc))
  (lu:assert-nil st-acc-1)
  nil)

(defmethod stage-acc= ((st-acc-1 stage-acc) (st-acc-2 list))
  (lu:assert-nil st-acc-2)
  nil)

(defmethod stage-acc= ((st-acc-1 stage-acc) (st-acc-2 stage-acc))
  (lu:assert-equal '(final gc-block
                  gc-state-0 gc-pre-state gc-main-state gc-post-state
                  staged-pre-cmds staged-main-cmds staged-post-cmds
                  pre-cmd main-cmd post-cmd)
                stage-acc-slots)
  (and (eql (final st-acc-1) (final st-acc-2))
       (equal (gc-block st-acc-1) (gc-block st-acc-2))
       (gc-state= (gc-state-0 st-acc-1) (gc-state-0 st-acc-2))
       (gc-state= (gc-pre-state st-acc-1) (gc-pre-state st-acc-2))
       (gc-state= (gc-main-state st-acc-1) (gc-main-state st-acc-2))
       (gc-state= (gc-post-state st-acc-1) (gc-post-state st-acc-2))
       (staged-cmds= (staged-pre-cmds st-acc-1) (staged-pre-cmds st-acc-2))
       (staged-cmds= (staged-main-cmds st-acc-1) (staged-main-cmds st-acc-2))
       (staged-cmds= (staged-post-cmds st-acc-1) (staged-post-cmds st-acc-2))
       (gc-cmd= (pre-cmd st-acc-1) (pre-cmd st-acc-2))
       (gc-cmd= (main-cmd st-acc-1) (main-cmd st-acc-2))
       (gc-cmd= (post-cmd st-acc-1) (post-cmd st-acc-2))))

