(in-package #:gcode)

;; :x  0.0 :y 0.0 :z 0.0 :f 0.0 :i 0.0 :j 0.0)

(defun collapse-whitespaces (s)
  "Ensure there is only one space character between words.
  Remove newlines."
  (ppcre:regex-replace-all "\\s+" s " "))

(defmethod parse-empty ((s string))
  (if (or (not s)
          (equal "" (str:trim s))
          (str:emptyp s))
      (make-gcode :gc-nil)
      nil))

(defmethod parse-comment ((s string))
  (let ((c (ppcre:regex-replace-all
            "^(n|N){1}\\s*\\d+\\s*" (str:trim s) "")))
    (if (or (string= "/" (str:s-first c))
            (and (string= "(" (str:s-first c))
                 (string= ")" (str:s-last c))))
        (make-gcode :gc-comment c)
        nil)))

(defmethod token-positions ((s string) token-letter-list)
  (loop for i from 0 to (1- (length s))
	when (member (aref (string-upcase s) i)
		     token-letter-list)
	  collect i))

(defun token-ranges (t-pos len-1)
  (cond ((null t-pos)
	 nil)
	((null (rest t-pos))
	 (list (list (first t-pos) len-1)))
	(t
	 (cons (list (first t-pos)
		     (1- (second t-pos)))
	       (token-ranges (rest t-pos) len-1)))))

(defmethod tokenize ((params string) token-letter-list)
  (let* ((s (str:upcase (str:replace-all " " "" params)))
         (t-pos-fn #'(lambda (s) (token-positions s token-letter-list)))
	 (ranges (token-ranges (funcall t-pos-fn s)
			       (1- (length s)))))
    (mapcar #'(lambda (r)
		(str:substring (first r) (1+ (second r)) s))
	    ranges)))

(defmethod as-float ((s string))
  (let ((z (multiple-value-bind (l u)
	       (ppcre:scan "^((\\+|-){0,1}(\\d+|\\d+(\\.\\d*)|\\d*(\\.\\d+)))$" s)
	     (if (and (null l) (null u)) nil (list l u)))))
    (if z (read-from-string s) nil)))

(defmethod is-int? ((s string))
   (ppcre:scan "^((\\+|-){0,1}\\d+)$" s))

(defmethod is-uint? ((s string))
  (ppcre:scan "^\\d+$" s))

(defmethod char-pos ((s string) c)
  (let ((j -1))
    (loop for i across s
          do (setf j (1+ j))
          when (char= i c) collect j)))

(defmethod first-pos? ((s string) c)
  (let ((pos (char-pos s c)))
    (if pos (apply #'min pos) nil)))

(defmethod last-pos? ((s string) c)
  (let ((pos (char-pos s c)))
    (if pos (apply #'max pos) nil)))

(defmethod remove-comment ((s string))
  (let ((f (first-pos? s #\())
        (l (last-pos? s #\))))
    (if (and f l (< f l))
        (str:replace-all (str:substring f (1+ l) s) "" s)
        s)))

(defmethod parse-xyz ((s string))
  (if (string= "" (collapse-whitespaces s))
      (make-gcode :gc-nil)
      nil))

(defmethod missing-float-value-p ((token string))
  (and (eql 1 (length token))
       (member (aref token 0) implemented-float-token)))

(defmethod valid-fps-token-p ((token string))
  (if (string= "" token)
      nil
      (let* ((l (aref token 0))
             (r (str:s-rest token))
             (f (as-float r)))
        (cond ((str:emptyp r) nil)
              ((and (member l implemented-float-token) f))))))

(defmethod valid-token? ((fp float-params) (token string))
  (let ((f (valid-fps-token-p token)))
    (cond ((member token implemented-cmds :test #'equal)
           (§:make-upcase-keyword token))
          (f (let ((l (aref token 0)))
               (cond ((char= l #\X) (setf (x fp) f))
                     ((char= l #\Y) (setf (y fp) f))
                     ((char= l #\Z) (setf (z fp) f))
                     ((char= l #\I) (setf (i fp) f))
                     ((char= l #\J) (setf (j fp) f))
                     ((char= l #\K) (setf (k fp) f))
                     ((char= l #\F) (setf (f fp) f))
                     ((char= l #\S) (setf (s fp) f)))))
          ;;(with-slots ('ls) fp (setf ls f))))
          (t nil))))

(defmethod normalize-short-cmds ((s string))
  (if (and (= 2 (length s))
           (member (aref s 0) short-cmds-letter))
      (format nil "~a0~a" (aref s 0) (aref s 1))
      s))

(defmethod implementedp ((token string))
  (or (member (str:s-first token) implemented-float-token)
      (member token implemented-cmds)))

(defmethod stage-1 ((st-acc stage-acc))
  "Stage 1 - Pre-flight check of a Gcode block

- use previous gc-state-0 or initialize gc-state-0
- discard lower and upper control char ranges silently
- GC-NIL on empty block
  - gc-state-0 is previous state for subsequent block
- GC-COMMENT on comment block, also if numbered
  - gc-state-0 is previous state for subsequent block"
  (unless (final st-acc)
    (let* ((state-0 (or (gc-state-0 st-acc)
                        (make-instance 'gc-state)))
           (s (§:standard-str (gc-block st-acc)))
           (p-e (parse-empty s))
           (p-c (parse-comment s)))
      (cond (p-e
             (setf (pre-cmd st-acc) p-e)
             (setf (final st-acc) :stage-1)) 
            (p-c
             (setf (pre-cmd st-acc) p-c)
             (setf (final st-acc) :stage-1)))
      (setf (gc-state-0 st-acc) state-0)))
  st-acc)

(defmethod stage-2 ((st-acc stage-acc))
  "Stage 2 - Tokenize a Gcode block

- remove inline comments
- trim whitespace
- tokenize Gcode block
  - uppercased words
  - float parameter words grouped with values
  - discard not implemented words silently
    - formal check on first letter of word
  - discard line numbering
  - preserve word sequence
- GC-ERROR on missing float values
  - gc-state-0 is previous state for error handling"
  (unless (final st-acc)
      (let* ((s (gc-block st-acc))
             (t-s (mapcar #'normalize-short-cmds
                          (§:fold-fns (list  #'(lambda (s)
                                                 (tokenize s implemented-token))
                                             #'str:trim
                                             #'remove-comment)
                                      s))))
        (setf (staged-main-cmds st-acc) t-s)
        (when (loop for tn in t-s
                    when (missing-float-value-p tn) collect tn)
          (setf (final st-acc) :stage-2)
          (setf (pre-cmd st-acc)
                (make-gc-error
                 (format nil "([ERROR] ~a : missing float value)"
                         (gc-block st-acc)))))))
  st-acc)

(defmethod pre-g-cmds ((l list) (p list))
  (cond ((null l)
         (list (reverse p) nil))
        ((not (member (first l) implemented-motion-cmds :test #'equal)) 
         (pre-g-cmds (rest l) (cons (first l) p)))
        (t (list (reverse p) l))))

(defmethod post-g-cmds ((l list) (l-g list))
  (cond ((null l)
         (let ((r (list (reverse l-g) nil)))
           (if (equal '(nil nil) r) nil r)))
        ((char/= #\M (aref (first l) 0))
         (post-g-cmds (rest l) (cons (first l) l-g)))
        (t (list (reverse l-g) l))))

(defmethod redundant-cmd-p ((state gc-state) cmd)
  (unless (spindle state)
    (setf (spindle state) (make-instance 'spindle-state)))
  (let ((sp-state (spindle state)))
    (labels ((eql-cmd (c-kw st-kw st-slot)
               (and (eql c-kw cmd) (eql st-kw st-slot))))
      (or (and (member cmd '(:M00 :M01 :M02 :M30) :test #'equal)
               (not (motor state)))
          (eql-cmd :m03 :cw (spin sp-state))
          (eql-cmd :m04 :ccw (spin sp-state))
#|
          (and (eql cmd :m05)
               (not (spin sp-state)))
|#
          (eql-cmd :m07 :on (mist sp-state))
          (eql-cmd :m08 :on (flood sp-state))
          (and (eql cmd :m09)
               (not (mist sp-state))    
               (not (flood sp-state)))
          (eql-cmd :g00 :jog (motion state))
          (eql-cmd :g01 :lin (motion state))
          (eql-cmd :g02 :cw (motion state))
          (eql-cmd :g03 :ccw (motion state))
          (eql-cmd :g20 :inch (unit state))
          (eql-cmd :g21 :mm (unit state))
          (eql-cmd :g90 :abs (mode state))
          (eql-cmd :g91 :inc (mode state))))))

(defmethod drop-cmd-p ((state gc-state))
  (lambda (cmd)
    (and (redundant-cmd-p state cmd)
         (not (member cmd '(:G00 :G01 :G03 :G04) :test #'equal)))))

(defun tokenize-implemented-cmds (t-s) 
  (let* ((fps (make-instance 'float-params))
         (cmds (§:fold-fns (list #'(lambda (l) (if (listp l)
                                                   (remove-if #'numberp l) nil))
                                 #'(lambda (l)
                                     (if (listp l)
                                         (loop for i in l
                                               collect (valid-token? fps i))
                                         nil)))
                           t-s)))
    (list cmds fps)))

;; pick-cmd
;; if g17/18/19 -> ijk, if i or j or k
;;              -> zyx -> implies circle (helix needs new zyx

(defun count-members (sub super test)
  (reduce (lambda (k c) (if (member k super :test test) (1+ c) c))
          sub :from-end t :initial-value 0))

(defun modal-g-ok (ks)
  (labels ((ok (ks m-g) (> 2 (count-members ks m-g #'equal)))) 
    (and (ok ks modal-group-1)
         (ok ks modal-group-2)
         (ok ks modal-group-3)
         (ok ks modal-group-5)
         (ok ks modal-group-6)
         (ok ks modal-group-7)
         (ok ks modal-group-8)
         (ok ks modal-group-10)
         (ok ks modal-group-12)
         (ok ks modal-group-13))))

(defun modal-m-ok (ks)
  (labels ((ok (ks m-g) (> 2 (count-members ks m-g #'equal)))) 
    (and (ok ks modal-group-4-m)
         (ok ks modal-group-6-m)
         (ok ks modal-group-7-m)
         (ok ks modal-group-8-m)
         (ok ks modal-group-9-m))))

(defmethod stage-3 ((st-acc stage-acc))
  "Stage 3 - Break up a Gcode block

- split word word list in three parts
  - pivot on first implemented motion word G0-3, G17-19
  - until, excluding a M word
  - from excluded M word
- for each part
  - separate float parameter words from other words 
  - check modal constraints for G words
  - check modal constraints for M words
- GC-ERROR on any modal constraint violation
  - gc-state-0 is previous state for error handling"
  (unless (final st-acc)
      (labels ((m-st-cmds (l)
                 (make-instance 'staged-cmds :cmds (first l) :fps (second l))))
        (let* ((tokenized-cmds (staged-main-cmds st-acc))
               (pre-cmd-split (pre-g-cmds (if (listp tokenized-cmds)
                                              tokenized-cmds
                                              (list tokenized-cmds))
                                          nil))
               (post-cmd-split (post-g-cmds (if (listp (second pre-cmd-split))
                                                (second pre-cmd-split)
                                                (list (second pre-cmd-split)))
                                            nil)))
          (setf (staged-pre-cmds st-acc)
                (m-st-cmds (tokenize-implemented-cmds (first pre-cmd-split))))
          (setf (staged-main-cmds st-acc)
                (m-st-cmds (tokenize-implemented-cmds (first post-cmd-split))))
          (setf (staged-post-cmds st-acc)
                (m-st-cmds (tokenize-implemented-cmds (second post-cmd-split))))
          (cond ((and (modal-g-ok (cmds (staged-pre-cmds st-acc)))
                      (modal-m-ok (cmds (staged-pre-cmds st-acc)))
                      (modal-g-ok (cmds (staged-main-cmds st-acc)))
                      (modal-m-ok (cmds (staged-main-cmds st-acc)))
                      (modal-g-ok (cmds (staged-post-cmds st-acc)))
                      (modal-m-ok (cmds (staged-post-cmds st-acc)))))
                (t (setf (final st-acc) :stage-3)
                   (setf (pre-cmd st-acc)
                         (make-gc-error
                          (format nil
                                  "([ERROR] ~a : modal constraint violated)"
                                  (gc-block st-acc)))))))))
  st-acc)

(defmethod is-unit? ((st gc-state) (cmds list))
  (or (unit st)
      (when (member :G20 cmds :test #'equal) :inch)
      (when (member :G21 cmds :test #'equal) :mm)))

(defmethod is-unit? ((st list) (cmds list))
  (assert (endp st))
  (or (when (member :G20 cmds :test #'equal) :inch)
      (when (member :G21 cmds :test #'equal) :mm)))

(defmethod is-mode? ((st gc-state) (cmds list))
  (or (mode st)
      (when (member :G90 cmds :test #'equal) :abs) 
      (when (member :G91 cmds :test #'equal) :inc)))

(defmethod is-mode? ((st list) (cmds list))
  (assert (endp st))
  (or (when (member :G90 cmds :test #'equal) :abs)
      (when (member :G91 cmds :test #'equal) :inc)))

(defmethod linear-motion-fparams-p ((fps list))
  (assert (endp fps))
  nil)

(defmethod linear-motion-fparams-p ((fps float-params))
  (and (or (x fps) (y fps) (z fps))
       (reduce #'(lambda (p s) (and p (null (slot-value fps s))))
               woc-inc-slots)))

(defmethod is-linear-motion? ((st list) (st-cmds list))
  (assert (endp st))
  (assert (endp st-cmds))
  nil)

(defmethod is-linear-motion? ((st list) (st-cmds staged-cmds))
  (assert (endp st))
  (let* ((fps (fps st-cmds))
         (cmds (cmds st-cmds)))
    (if (and (linear-motion-fparams-p fps)
             (or (member :G00 (cmds st-cmds) :test #'equal)
                 (member :G01 (cmds st-cmds) :test #'equal))
             (or (member :G20 (cmds st-cmds) :test #'equal)
                 (member :G21 (cmds st-cmds) :test #'equal))
             (or (member :G90 (cmds st-cmds) :test #'equal)
                 (member :G91 (cmds st-cmds) :test #'equal))
             (is-mode? st cmds))
        (make-gc-motion cmds fps)
        nil)))

(defmethod is-linear-motion? ((st gc-state) (st-cmds list))
  (assert (endp st-cmds))
  nil)

(defmethod is-linear-motion? ((st gc-state) (st-cmds staged-cmds))
  (let* ((fps (fps st-cmds))
         (has-g00-p (member :G00 (cmds st-cmds) :test #'equal))
         (has-g01-p (member :G01 (cmds st-cmds) :test #'equal))
         (inherits-g00-p (and (not has-g00-p)
                              (not has-g01-p)
                              (eql :jog (motion st))))
         (inherits-g01-p (and (not has-g00-p)
                              (not has-g01-p)
                              (eql :lin (motion st))))
         (cmds (cond (inherits-g00-p (append (cmds st-cmds) '(:G00)))
                     (inherits-g01-p (append (cmds st-cmds) '(:G01)))
                     (t (cmds st-cmds)))))
    (if (and (linear-motion-fparams-p fps)
             (or has-g00-p has-g01-p inherits-g00-p inherits-g01-p)
             (is-unit? st cmds)
             (is-mode? st cmds))
        (make-gc-motion cmds fps)
        nil)))

(defmethod is-multi-command? ((st-cmds list))
  (lu:assert-nil st-cmds)
  nil)

(defmethod is-multi-command? ((st-cmds staged-cmds))
  (if (and (not (cmds st-cmds))
           (or (not (fps st-cmds))
               (float-params= (fps st-cmds)
                              (make-instance 'float-params))))
      nil
      (make-gc-multi-cmd (cmds st-cmds)
                         (fps st-cmds))))

;; prune step by step over staged-pre-cmds, staged-cmds    staged-post-cmds
;;                         gc-pre-state     gc-main-state  gc-post-state
(defmethod prune-cmds ((st gc-state) (st-cmds staged-cmds))
  (let ((r (remove-if (drop-cmd-p st) (cmds st-cmds))))
    (if (and (fps st-cmds)
             (or (x (fps st-cmds))
                 (y (fps st-cmds))
                 (z (fps st-cmds)))
             (not (member :g00 r :test #'equal))
             (not (member :g01 r :test #'equal))
             (not (member :g02 r :test #'equal))
             (not (member :g03 r :test #'equal)))
        (cond ((eql :jog (motion st)) (append r '(:G00)))
              ((eql :lin (motion st)) (append r '(:G01)))
              (t r))
        r)))

(defmethod update-gc-prev-unit ((st gc-state) (gc-c gc-cmd))
  (let ((cl (gc-list gc-c)))
    (cond ((member :g20 cl :test #'equal)
           (setf (unit st) :inch))
          ((member :g21 cl :test #'equal)
           (setf (unit st) :mm))))
  st)

(defmethod current-woc-pos ((st gc-state)) (woc-pos st))

(defmethod update-woc-pos ((st list) (fps list))
  (assert (endp st))
  (assert (endp fps))
  nil)

(defmethod update-woc-pos ((st gc-state) (fps list))
  (assert (endp fps))
  st)

(defmethod update-woc-pos ((st gc-state) (fps float-params))
  (unless (or (eql :mm (unit st))
              (eql :inch (unit st)))
    (setf (unit st) :mm))
  (with-accessors ((f-x x) (f-y y) (f-z z)) fps
    (let* ((wp (woc-pos st))
           (w (if wp wp (make-instance 'point-3 :x 0 :y 0 :z 0))))
      (with-accessors ((w-x x) (w-y y) (w-z z)) w
        (if (eql :inc (mode st))
            (progn
              (when (numberp f-x) (setf w-x (+ f-x (if (numberp w-x) w-x 0))))
              (when (numberp f-y) (setf w-y (+ f-y (if (numberp w-y) w-y 0))))
              (when (numberp f-z) (setf w-z (+ f-z (if (numberp w-z) w-z 0)))))
            (progn
              (when (numberp f-x) (setf w-x f-x))
              (when (numberp f-y) (setf w-y f-y))
              (when (numberp f-z) (setf w-z f-z))))
        (setf (woc-pos st) w)
        )))
  st)

(defmethod update-gc-prev-motion ((st gc-state) (gc-c gc-cmd))
  (let ((cl (gc-list gc-c)))
    (cond ((or (member :g0 cl) (member :g00 cl)) 
           (setf (motion st) :jog))
          ((or (member :g1 cl) (member :g01 cl))
           (setf (motion st) :lin))
          ((or (member :g2 cl) (member :g02 cl))
           (setf (motion st) :cw))
          ((or (member :g3 cl) (member :g03 cl))
           (setf (motion st) :ccw)))
    st))

(defmethod update-gc-prev-mode ((st gc-state) (gc-c gc-cmd))
  (let ((cl (gc-list gc-c)))
    (cond ((member :g90 cl)
           (setf (mode st) :abs))
          ((member :g91 cl)
           (setf (mode st) :inc)))
    st))

(defmethod update-gc-state ((st gc-state) (gc-c gc-cmd))
  (let* ((st1 (update-woc-pos st (f-params gc-c)))
         (st2 (update-gc-prev-unit st1 gc-c))
         (st3 (update-gc-prev-motion st2 gc-c))
         (st4 (update-gc-prev-mode st3 gc-c)))
    st4))

(defmethod convert-units ((st gc-state) (st-cmds list))
  st-cmds)

(defmethod convert-units ((st gc-state) (st-cmds staged-cmds))
  (when (or (member :G20 (cmds st-cmds))
            (eql :inch (unit st)))
    (let ((fps (fps st-cmds)))
      (when fps
        (let ((r (clone-by-slots fps float-params-slots))
              (r2 (remove-if-not #'second
                                 (loop for f in convertible-slots
                                       collect (list f (eval (slot f fps)))))))
          (loop for (f v) in r2 do (eval (set-slot f r (* 25.4 v))))
          (setf (fps st-cmds) r)))))
  st-cmds)

(defmethod stage-4 ((st-acc stage-acc))
  "Stage 4 - Analyze pre-command

- TODO update spindle state
- TODO update feed state
- convert inch to mm in pre-command
  - gc-state-0 unit is :inch
  - staged pre-command list contains :G20
- prune words, if effectless on gc-state-0
  - keep jog/linear motion word
  - add jog/linear motion word according to gc-state-0, if missing in block
- GC-NIL
  - empty pre-commands list
  - empty float parameter object, i.e. all slots have value nil
  - gc-pre-state is previous state for subsequent stage
- GC-LINEAR-MOTION 
  - jog/linear motion word derived from gc-state-0
    - holds implicitely because of stage 3
    - jog/linear motion word added from gc-state-0 
  - no incremental float parameters, ref. woc-inc-slots
  - update gc-pre-state
    - update woc position
    - assume default unit mm
  - previous state for subsequent block is undefined
  - gc-pre-state is previous state for subsequent stage
- GC-MULTI-CMD 
  - no jog/linear motion word
  - no motion float parameters x, y, z
  - no incremental float parameters, ref. woc-inc-slots
  - update gc-pre-state
  - gc-pre-state is previous state for subsequent stage
  - previous state for subsequent block is undefined
- GC-ERROR on incomplete linear motion pre-command
  - motion float parameters are present
  - no jog/linear motion word derived from gc-state-0
  - gc-state-0 is previous state for error handling"
  (unless (final st-acc)
    (let ((st-gc-state-0 (gc-state-0 st-acc))
          (st-staged-pre-cmds (staged-pre-cmds st-acc)))
      (setf (cmds (staged-pre-cmds st-acc))
            (convert-units st-gc-state-0
                           (prune-cmds st-gc-state-0 st-staged-pre-cmds)))
      (let ((r (is-linear-motion? st-gc-state-0 st-staged-pre-cmds))
            (r2 (is-multi-command? st-staged-pre-cmds)))
        (cond ((and (endp (cmds st-staged-pre-cmds))
                    (float-params= (make-instance 'float-params)
                                   (fps st-staged-pre-cmds)))
               (setf (pre-cmd st-acc) (make-gcode :gc-nil))
               (setf (gc-pre-state st-acc) st-gc-state-0))
              (r (setf (pre-cmd st-acc) r)
                 (setf (gc-pre-state st-acc)
                       (update-gc-state st-gc-state-0 r)))
              ((linear-motion-fparams-p (fps st-staged-pre-cmds))
               (setf (pre-cmd st-acc)
                     (make-gc-error
                      (format nil
                              "([ERROR] ~a : incomplete motion pre-command)"
                              (gc-block st-acc))))
               (setf (final st-acc) :stage-4))
              (r2 (setf (pre-cmd st-acc) r2)
                  (setf (gc-pre-state st-acc)
                        (update-gc-state st-gc-state-0 r2)))
              (t (setf (pre-cmd st-acc)
                       (make-gc-error
                        (format nil
                                "([ERROR] ~a : invalid pre-command)"
                                (gc-block st-acc))))
                 (setf (final st-acc) :stage-4))))))
  st-acc)
    
(defmethod is-xy-helix? ((st-cmds staged-cmds))
  (let ((cmds (cmds st-cmds))
        (fps (fps st-cmds)))
    (cond ((null fps) nil)
          ((not (or (member :G02 cmds) (member :G03 cmds))) nil)
          ((or (member :G18 cmds) (member :G19 cmds)) nil)
          ((and (x fps) (y fps) (i fps) (j fps) (z fps)
                (null (k fps)) (null (r fps)))
           (make-gc-helix (unless (member :G17 cmds) (cons :G17 cmds)) fps))
          (t nil))))

(defmethod is-xz-helix? ((st-cmds staged-cmds))
  (let ((cmds (cmds st-cmds))
        (fps (fps st-cmds)))
    (cond ((null fps) nil)
          ((not (or (member :G02 cmds) (member :G03 cmds))) nil)
          ((or (member :G17 cmds) (member :G19 cmds)) nil)
          ((and (x fps) (z fps) (i fps) (k fps) (y fps)
                (null (j fps)) (null (r fps)))
           (make-gc-helix (unless (member :G18 cmds) (cons :G18 cmds)) fps))
          (t nil))))

(defmethod is-yz-helix? ((st-cmds staged-cmds))
  (let ((cmds (cmds st-cmds))
        (fps (fps st-cmds)))
    (cond ((null fps) nil)
          ((not (or (member :G02 cmds) (member :G03 cmds))) nil)
          ((or (member :G17 cmds) (member :G18 cmds)) nil)
          ((and (y fps) (z fps) (j fps) (k fps) (x fps)
                (null (i fps)) (null (r fps)))
           (make-gc-helix (unless (member :G19 cmds) (cons :G19 cmds)) fps))
          (t nil))))

(defmethod is-xy-circle? ((st-cmds staged-cmds))
  (let ((cmds (cmds st-cmds))
        (fps (fps st-cmds)))
    (cond ((null fps) nil)
          ((not (or (member :G02 cmds) (member :G03 cmds))) nil)
          ((or (member :G18 cmds) (member :G19 cmds)) nil)
          ((and (x fps) (y fps) (i fps) (j fps)
                (null (z fps)) (null (k fps)) (null (r fps)))
           (make-gc-circle (unless (member :G17 cmds) (cons :G17 cmds)) fps))
          (t nil))))

(defmethod is-xz-circle? ((st-cmds staged-cmds))
  (let ((cmds (cmds st-cmds))
        (fps (fps st-cmds)))
    (cond ((null fps) nil)
          ((not (or (member :G02 cmds) (member :G03 cmds))) nil)
          ((or (member :G17 cmds) (member :G19 cmds)) nil)
          ((and (x fps) (z fps) (i fps) (k fps)
                (null (y fps)) (null (j fps)) (null (r fps)))
           (make-gc-circle (unless (member :G18 cmds) (cons :G18 cmds)) fps))
          (t nil))))

(defmethod is-yz-circle? ((st-cmds staged-cmds))
  (let ((cmds (cmds st-cmds))
        (fps (fps st-cmds)))
    (cond ((null fps) nil)
          ((not (or (member :G02 cmds) (member :G03 cmds))) nil)
          ((or (member :G17 cmds) (member :G18 cmds)) nil)
          ((and (y fps) (z fps) (j fps) (k fps)
                (null (x fps)) (null (i fps)) (null (r fps)))
           (make-gc-circle (unless (member :G19 cmds) (cons :G19 cmds)) fps))
          (t nil))))

(defmethod circle-woc-pos ((st gc-state) (gc-c gc-circle))
  (let* ((c (or (is-xy-circle? gc-c) (is-xz-circle? gc-c) (is-yz-circle? gc-c)))
         (cl (gc-list gc-c))
         (x-3 (woc-pos st))
         (fp (f-params st))
         (x (or (x fp) (x x-3)))
         (y (or (y fp) (y x-3)))
         (z (or (z fp) (z x-3))))
    (cond ((member :g17 cl)
           nil)
          ((member :g18 cl)
           nil)
          ((member :g19 cl)
           nil)
          (t (error "circle-woc-pos failed on GCode circle command")))))

(defmethod stage-5 ((st-acc stage-acc))
  "Stage 5 - Analyze main command

- TODO update spindle state
- TODO update feed state
- convert inch to mm in main-command
  - gc-pre-state unit is :inch
  - staged main-command list contains :G20
- prune words, if effectless on gc-pre-state
  - keep jog/linear motion word
  - add jog/linear motion word according to gc-main-state, if missing in block
- GC-NIL
  - empty main commands list
  - empty float parameter object, i.e. all slots have value nil
  - gc-main-state is previous state for subsequent stage
- GC-LINEAR-MOTION 
  - jog/linear motion word present in main commands list
  - jog/linear motion word derived from gc-pre-state
    - jog/linear motion word added from gc-pre-state 
    - no incremental float parameters, ref. woc-inc-slots
  - update gc-main-state
    - update woc position
    - assume default unit mm
  - previous state for subsequent block is undefined
  - gc-main-state is previous state for subsequent stage
- GC-CIRCLE, GC-HELIX 
  - cw/ccw motion word present in main commands list
  - cw/ccw motion word derived from gc-pre-state
    - cw/ccw motion word added from gc-pre-state
    - plane specifier word added 
  - incremental float parameters according to type and plane
  - update gc-main-state
    - update woc position
    - assume default unit mm
  - previous state for subsequent block is undefined
  - gc-main-state is previous state for subsequent stage
- GC-ERROR on GC-MULTI-CMD 
  - invalid for stage 5
  - gc-pre-state is previous state for error handling
  - previous state for subsequent block is undefined
    - TODO propagate gc-pre-state to gc-main-state?
- GC-NIL
  - TODO is pre-cmd/post-cmd/no main-cmd a reasonable use case? 
  - empty staged-main-cmds
  - propagate gc-pre-state to gc-main-state
  - gc-main-state is previous state for subsequent stage
  - previous state for subsequent block is undefined
- GC-ERROR on incomplete linear motion main-command
  - motion float parameters are present
  - no jog/linear motion word derived from gc-pre-state
  - gc-pre-state is previous state for error handling
    - TODO propagate gc-pre-state to gc-main-state?
- GC-ERROR on any other staged main commands 
  - gc-main-state is previous state for error handling
    - TODO propagate gc-main-state to gc-post-state?"
  (unless (final st-acc)
    (let* ((st-gc-pre-state (gc-pre-state st-acc))
           (st-staged-main-cmds (convert-units st-gc-pre-state
                                               (staged-main-cmds st-acc))))
      (setf (cmds (staged-main-cmds st-acc))
            (prune-cmds st-gc-pre-state st-staged-main-cmds))
      (let ((r (or (is-xy-circle? st-staged-main-cmds)
                   (is-xz-circle? st-staged-main-cmds)
                   (is-yz-circle? st-staged-main-cmds)
                   (is-xy-helix? st-staged-main-cmds)
                   (is-xz-helix? st-staged-main-cmds)
                   (is-yz-helix? st-staged-main-cmds)
                   (is-linear-motion? st-gc-pre-state st-staged-main-cmds)))
            (r2 (is-multi-command? st-staged-main-cmds)))
        (cond ((and (endp (cmds st-staged-main-cmds))
                    (float-params= (make-instance 'float-params)
                                   (fps st-staged-main-cmds)))
               (setf (main-cmd st-acc) (make-gcode :gc-nil))
               (setf (gc-main-state st-acc) st-gc-pre-state))
              (r (setf (main-cmd st-acc) r)
                 (setf (gc-main-state st-acc)
                       (update-gc-state (gc-pre-state st-acc) r)))
              ((and (endp r)
                    (linear-motion-fparams-p (fps st-staged-main-cmds)))
               (setf (main-cmd st-acc)
                     (make-gc-error
                      (format nil
                              "([ERROR] ~a : incomplete linear motion main command)"
                              (gc-block st-acc))))
               (setf (final st-acc) :stage-5))
              (r2 (setf (main-cmd st-acc)
                        (make-gc-error
                         (format nil
                                 "([ERROR] ~a : multi command is not permitted)"
                                 (gc-block st-acc))))
                  (setf (final st-acc) :stage-5))
              (t (setf (main-cmd st-acc)
                       (make-gc-error
                        (format nil
                                "([ERROR] ~a : unknown command)"
                                (gc-block st-acc))))
                 (setf (final st-acc) :stage-5))))))
  st-acc)

(defmethod stage-6 ((st-acc stage-acc))
  "Stage 6 - Analyze post command

- TODO update spindle state
- TODO update feed state
- convert inch to mm in post-command
  - gc-main-state unit is :inch
  - staged post-command list contains :G20
- prune words, if effectless on gc-main-state
  - keep jog/linear motion word
  - add jog/linear motion word according to gc-main-state, if missing in block
- GC-NIL
  - empty main commands list
  - empty float parameter object, i.e. all slots have value nil
  - gc-main-state is previous state for subsequent stage
- GC-LINEAR-MOTION 
  - jog/linear motion word present in post commands list
  - jog/linear motion word derived from gc-main-state
    - jog/linear motion word added from gc-main-state 
    - no incremental float parameters, ref. woc-inc-slots
  - update gc-post-state
    - update woc position
    - assume default unit mm
- GC-MULTI-CMD 
  - no jog/linear motion word
  - no motion float parameters x, y, z
  - no incremental float parameters, ref. woc-inc-slots
  - update gc-post-state
    - update woc position
    - assume default unit mm
- GC-NIL
  - empty staged-post-cmds
  - propagate gc-main-state to gc-post-state
- GC-ERROR on any other staged post commands"
  (unless (final st-acc)
    (setf (final st-acc) :stage-6)
    (let* ((st-gc-main-state (gc-main-state st-acc))
           (st-staged-post-cmds (convert-units st-gc-main-state
                                               (staged-post-cmds st-acc))))
      (setf (cmds st-staged-post-cmds)  
            (prune-cmds st-gc-main-state st-staged-post-cmds))
      (let ((r (or (is-linear-motion? st-gc-main-state st-staged-post-cmds)
                   (is-multi-command? st-staged-post-cmds))))
        (cond ((and (endp (cmds st-staged-post-cmds))
                    (float-params= (make-instance 'float-params)
                                   (fps st-staged-post-cmds)))
               (setf (post-cmd st-acc) (make-gcode :gc-nil))
               (setf (gc-post-state st-acc) st-gc-main-state))
              (r (setf (post-cmd st-acc) r)
                 (setf (gc-post-state st-acc)
                       (update-gc-state (gc-main-state st-acc) r)))
              (t (setf (post-cmd st-acc)
                       (make-gc-error
                        (format nil
                                "([ERROR] ~a : unknown command)"
                                (gc-block st-acc)))))))))
  st-acc)


(defmethod stage-7 ((st-acc stage-acc))
  "Stage 7 - Update CNC state

"
  (let ((st-acc-7 (make-stage-acc ""))
        (final (final st-acc)))
    (assert st-acc-7)
    (cond ((or (eql :stage-1 final)
               (eql :stage-2 final)
               (eql :stage-3 final)
               (eql :stage-4 final)) 
           (assert (pre-cmd st-acc))
           (setf (pre-cmd st-acc-7) (pre-cmd st-acc))
           (setf (gc-state-0 st-acc-7) (gc-state-0 st-acc))
           (setf (gc-block st-acc-7) (gc-block st-acc)))
          ((eql :stage-5 final)
           (assert (pre-cmd st-acc))
           (assert (main-cmd st-acc))
           (setf (pre-cmd st-acc-7) (pre-cmd st-acc))
           (setf (main-cmd st-acc-7) (main-cmd st-acc))
           (setf (gc-state-0 st-acc-7) (gc-main-state st-acc))
           (setf (gc-block st-acc-7) (gc-block st-acc)))
          ((eql :stage-6 final)
           (assert (pre-cmd st-acc))
           (assert (main-cmd st-acc))
           (assert (main-cmd st-acc))
           (setf (pre-cmd st-acc-7) (pre-cmd st-acc))
           (setf (main-cmd st-acc-7) (main-cmd st-acc))
           (setf (post-cmd st-acc-7) (post-cmd st-acc))
           (setf (gc-state-0 st-acc-7) (gc-post-state st-acc))
           (setf (gc-block st-acc-7) (gc-block st-acc)))
          (t (setf st-acc-7 nil)))
    st-acc-7))

