
#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(ql:quickload 'str)
(ql:quickload 'lisp-unit)
(ql:quickload 'cl-ppcre)
(ql:quickload 'misc)

(load "package.lisp")

(load "src/data-utils.lisp")

(load "src/gcode-const.lisp")
(load "src/gcode-data.lisp")
(load "src/gcode.lisp")

(load "t/gcode-tests.lisp")

