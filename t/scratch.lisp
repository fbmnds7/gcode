(in-package #:cnc-host/gcode)

(defparameter gc-block "N123 X4 S10000 M3 G20 G90 G1 X3 y-.02 Z45 F4 M5")

(defparameter gc-state-0 (make-instance 'gc-state :unit :mm
                                                  :motion :jog
                                                  :mode :abs))

(defparameter s0 (make-instance 'stage-acc
                                :gc-block gc-block
                                :gc-state-0 gc-state-0))

(format-stage-acc s0)

(defparameter s1 (stage-1 s0))

(defparameter s2 (stage-2 s1))

(defparameter s3 (stage-3 s2))

(defparameter s4 (stage-4 s3))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(in-package #:cnc-host/gcode)

(defparameter gc-state-0 (make-instance 'gc-state
                                        :unit :inch
                                        :mode :abs))

(defparameter nc "M03 x -3 i1 S24000")

(defparameter gc-st-0 (make-instance 'stage-acc                        
                                     :gc-block nc        
                                     :gc-state-0 gc-state-0))
 
(defparameter st-acc (§:fold-fns '(stage-6
                                   stage-5
                                   stage-4
                                   stage-3
                                   stage-2
                                   stage-1)
                                 gc-st-0))

(defparameter st-acc-1 (§:fold-fns '(stage-1) gc-st-0))

(defparameter st-acc-2 (§:fold-fns '(stage-2 stage-1) gc-st-0))

(defparameter st-acc-3 (§:fold-fns '(stage-3 stage-2 stage-1) gc-st-0))

(defparameter st-acc-4 (§:fold-fns '(stage-4 stage-3 stage-2 stage-1) gc-st-0))

(defmethod convert-units ((st gc-state) (st-cmds staged-cmds))
  (when (or (member :G20 (cmds st-cmds))
            (eql :inch (unit st)))
    (let ((fps (fps st-cmds))
           (r (make-instance 'float-params))
          (r2 (remove-if-not #'second
                             (loop for f in float-params-slots
                                   collect (list f (eval (slot f fps)))))))
      (loop for (f v) in r2 do (eval (set-slot f r (* 25.4 v))))
      (setf (fps st-cmds) r)))
  st-cmds)

(defparameter r (make-instance 'float-params))

(defparameter r2 (remove-if-not #'second
                                (loop for f in float-params-slots
                                      collect (list f (eval (slot f fps))))))

(let ((r (make-instance 'float-params))
      (r2 (remove-if-not #'second
                         (loop for f in float-params-slots
                               collect (list f (eval (slot f fps)))))))
  (loop for (f v) in r2 do (eval (set-slot f r (* 25.4 v)))))

;;; https://plaster.tymoon.eu/view/1740

;; author: Michał "phoe" Herda
;; license: MIT / attribute me and do whatever you want

(defmacro handler-case* (form &rest cases)
  "A variant of HANDLER-CASE, in which the case forms are evaluating before
performing a transfer of control. This ensures that the case forms are evaluated
in the dynamic scope of the signaling form."
  (let ((no-error-case-count (count :no-error cases :key #'car)))
    (case no-error-case-count
      (0 (make-handler-case*-without-no-error-case form cases))
      (1 (make-handler-case*-with-no-error-case form cases))
      (t (error "Multiple :NO-ERROR cases found in HANDLER-CASE*.")))))

(defun make-handler-case*-with-no-error-case (form cases)
  (let* ((no-error-case (assoc :no-error cases))
         (other-cases (remove no-error-case cases)))
    (let ((normal-return (gensym "NORMAL-RETURN"))
          (error-return  (gensym "ERROR-RETURN")))
      `(block ,error-return
         (multiple-value-call (lambda ,@(cdr no-error-case))
           (block ,normal-return
             (return-from ,error-return
               (handler-case* (return-from ,normal-return ,form)
                              ,@other-cases))))))))

(defun make-handler-case*-without-no-error-case (form cases)
  (let ((block-name (gensym "HANDLER-CASE*-BLOCK")))
    (flet ((make-handler-bind-case (case)
             (destructuring-bind (type lambda-list . body) case
               `(,type (lambda ,lambda-list
                         (return-from ,block-name (locally ,@body)))))))
      (let ((bindings (mapcar #'make-handler-bind-case cases)))
        `(block ,block-name (handler-bind ,bindings ,form))))))



