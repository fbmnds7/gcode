
(in-package #:gcode)

(setq lu:*print-failures* t)

(lu:define-test test-make-upcase-keyword ()
  (let ((k (§:make-upcase-keyword "test")))
    (lu:assert-eq :test k)
    (lu:assert-eq :TEST k)))

(lu:define-test test-token-positions ()
  (lu:assert-equal '(0 5 6 10)
                (token-positions "t1234tt789t"
                                 valid-token-letter)))

(lu:define-test test-token-ranges ()
  (let ((t-pos-fn #'(lambda (s)
                      (token-positions s valid-token-letter))))
    (lu:assert-nil (token-ranges (funcall t-pos-fn "e") 0))
    (lu:assert-equal '((0 0)) (token-ranges (funcall t-pos-fn "g") 0))
    (lu:assert-equal '((0 0)) (token-ranges (funcall t-pos-fn "x") 0))
    (let ((t-pos  (funcall t-pos-fn "t1234tt789t"))
          (len-1 (1- (length "t1234tt789t"))))
      (lu:assert-equal '((0 4) (5 5) (6 9) (10 10))
                    (token-ranges t-pos len-1)))))

(lu:define-test test-tokenize ()
  (lu:assert-equal '("G0" "X1" "Y2" "Z-3." "F44")
		(tokenize "g0 x 1 y 2 z -3. f 44"
                          valid-token-letter))
  (lu:assert-equal '("G0" "X" "Y2" "Z-3." "F44")
		(tokenize "g0 x  y 2 z -3. f 44"
                          valid-token-letter)))

(lu:define-test test-as-float ()
  (lu:assert-true (= 123 (as-float "123")))
  (lu:assert-true (= -123 (as-float "-123")))
  (lu:assert-true (= 123.45 (as-float "123.45")))
  (lu:assert-true (= 123.45 (as-float "+123.45")))
  (lu:assert-true (= 0.98 (as-float ".98")))
  (lu:assert-true (= -0.98 (as-float "-.98")))
  (lu:assert-true (= 0.98 (as-float ".98")))
  (lu:assert-true (= 0.98 (as-float "+.98")))
  (lu:assert-true (= 0 (as-float "0.")))
  (lu:assert-true (= 0 (as-float "-0.")))
  (lu:assert-true (= 0 (as-float "-.0")))
  (lu:assert-true (= 0 (as-float "+0.")))
  (lu:assert-true (= 0 (as-float "+.0"))))

(lu:define-test test-u/int ()
  (lu:assert-true (and (is-int? "-1")
                    (is-int? "+1")
                    (is-uint? "1")
                    (not (is-uint? "-1"))
                    (not (is-uint? "+1")))))

(lu:define-test test-pos-in-string ()
  (lu:assert-equal '(4 5) (char-pos "0123))6" #\)))
  (lu:assert-eql 4  (first-pos? "0123))6" #\)))
  (lu:assert-eql 5  (last-pos? "0123))6" #\)))
  (lu:assert-true (string= "0 " (remove-comment "0()1(345)) ")))
  (lu:assert-true (string= "" (remove-comment "(0()1(345))"))))

(lu:define-test test-valid-token? ()
  (let ((fp (make-instance 'float-params)))
    (lu:assert-nil (valid-token? fp "X"))
    (lu:assert-true (= -123.45 (valid-token? fp "X-123.45") (x fp)))
    (lu:assert-true (=  123.45 (valid-token? fp "F+123.45") (f fp)))
    (lu:assert-true (and (= -123.45 (x fp))
                      (=  123.45 (f fp))))))

(lu:define-test test-parse-comment ()
  (lu:assert-equal "(test)" (gc-text (parse-comment "N123 (test)")))
  (lu:assert-equal "(test)" (gc-text (parse-comment " N 123 (test)  ")))
  (lu:assert-nil (parse-comment " N+123 (test)  "))
  (lu:assert-nil (parse-comment " N +123 (test)  ")))

(lu:define-test test-fold-fns ()
  (lu:assert-equal "N123 g0 x1 Y2. z -3"
                   (§:fold-fns (list #'str:trim #'remove-comment)
                               "()  N123 g0 x1 Y2. z -3"))
  (lu:assert-equal "N123 g0 x1 Y2. z -3"
                   (§:fold-fns (list #'str:trim #'remove-comment)
                               "N123 g0 x1 Y2. ((test ))z -3"))
  (lu:assert-equal "N123 g0 x1 Y2. ((test z -3"
                   (§:fold-fns (list #'str:trim #'remove-comment)
                               "N123 g0 x1 Y2. ((test z -3")))

(lu:define-test test-stage-1 ()
  (let* ((st-acc (make-stage-acc "G00 Z5.0000"))
         (stage-1-st-acc (stage-1 st-acc)))
    (lu:assert-true (gc-state-0 st-acc))
    (lu:assert-equal "G00 Z5.0000"
                  (gc-block stage-1-st-acc))
    (lu:assert-false (final stage-1-st-acc))
    (lu:assert-nil (pre-cmd stage-1-st-acc)))
  (let* ((st-acc (make-stage-acc "N123 G0 X3 y-.02 Z45 F4"))
         (stage-1-st-acc (stage-1 st-acc)))
    (lu:assert-true (gc-state-0 st-acc))
    (lu:assert-equal "N123 G0 X3 y-.02 Z45 F4"
                  (gc-block stage-1-st-acc))
    (lu:assert-false (final stage-1-st-acc))
    (lu:assert-nil (pre-cmd stage-1-st-acc)))
  (let* ((st-acc (make-stage-acc "/N123 G0 X3 y-.02 Z45 F4"))
         (stage-1-st-acc (stage-1 st-acc)))
    (lu:assert-true (gc-state-0 st-acc))
    (lu:assert-equal "/N123 G0 X3 y-.02 Z45 F4"
                  (gc-block stage-1-st-acc))
    (lu:assert-true (final stage-1-st-acc))
    (lu:assert-true (gc-cmd= (pre-cmd stage-1-st-acc)
                          (make-gc-comment "/N123 G0 X3 y-.02 Z45 F4"))))
  (let* ((st-acc (make-stage-acc "N123 (G0 X3 y-.02 Z45 ) F4)"))
         (stage-1-st-acc (stage-1 st-acc)))
    (lu:assert-true (gc-state-0 st-acc))
    (lu:assert-equal "N123 (G0 X3 y-.02 Z45 ) F4)"
                  (gc-block stage-1-st-acc))
    (lu:assert-true (final stage-1-st-acc))
    (lu:assert-true  (gc-cmd= (pre-cmd stage-1-st-acc)
                           (make-gc-comment "(G0 X3 y-.02 Z45 ) F4)")))))

(lu:define-test test-stage-2 ()
  (let* ((gc-block "G00 Z5.0000") 
         (s2 (stage-2
              (stage-1
               (make-stage-acc gc-block)))))
    (lu:assert-nil (final s2))
    (lu:assert-nil (pre-cmd s2))
    (lu:assert-equal gc-block (gc-block s2)))
  (let* ((gc-block ":f") 
         (s2 (stage-2
              (stage-1
               (make-stage-acc gc-block)))))
    (lu:assert-true (final s2))
    (lu:assert-true (gc-cmd= (pre-cmd s2)
                          (make-gc-error
                           "([ERROR] :f : missing float value)")))
    (lu:assert-equal gc-block (gc-block s2)))
  (let* ((gc-block "N123 S10000 M3 G20 G90 G1 X3 y-.02 Z45 F4 M5")
         (s2 (stage-2
              (stage-1
               (make-stage-acc gc-block)))))
    (lu:assert-nil (final s2))
    (lu:assert-true (gc-state-0 s2))
    (lu:assert-equal '("S10000" "M03" "G20" "G90" "G01"
                    "X3" "Y-.02" "Z45" "F4" "M05")
                  (staged-main-cmds s2)))
  (let* ((gc-block "N123 G20 G90 G0 X3 y-.02 i-.0 j 3F4")
         (s2 (stage-2
             (stage-1
              (make-stage-acc gc-block)))))
    (lu:assert-nil (final s2))
    (lu:assert-true (gc-state-0 s2))
    (lu:assert-equal '("G20" "G90" "G00" "X3" "Y-.02" "I-.0" "J3" "F4")
                  (staged-main-cmds s2)))
  (let* ((gc-block "N123 G20 G90 G0 X3 y-.02 i-.0 j 3Z45 F4")
         (s2 (stage-2
             (stage-1
              (make-stage-acc gc-block)))))
    (lu:assert-nil (final s2))
    (lu:assert-true (gc-state-0 s2))
    (lu:assert-equal '("G20" "G90" "G00" "X3" "Y-.02" "I-.0" "J3" "Z45" "F4")
                  (staged-main-cmds s2))))

(lu:define-test test-copy-clone-format-by-slots ()
  (let* ((l '(gc-type gc-text))
         (a (make-gc-comment "text a"))
         (b (make-gc-comment "text b"))
         (c (gc-text b))
         (d (copy-by-slots a b l))
         (aa (clone-by-slots a l)))
    (lu:assert-equal "text b" c)
    (lu:assert-equal "text a" (gc-text b))
    (lu:assert-eq :gc-comment (gc-type aa))
    (lu:assert-equal "text a" (gc-text aa)))
  (let* ((st (make-instance 'gc-state :unit :mm))
         (f-b-s (format nil "~A"
                        (ppcre:regex-replace-all
                         #+sbcl "^#<GC-STATE \\{(\\d|[A-F])+\\}> "
                         #+ccl  "^#<GC-STATE #x(\\d|[A-F])+> "
                         #+ecl  "#<a .*:GC-STATE 0x(\\d|[a-f])+> \\(#<The "
                         (format-by-slots st gc-state-slots)
                         #+(or sbcl ccl) "#<GC-STATE {...}> "
                         #+ecl "#<GC-STATE {...}> (#<"))))
    (lu:assert-eq :mm (unit (clone-by-slots st gc-state-slots)))
    #-ccl (lu:assert-equal f-b-s
"#<GC-STATE {...}> (#<STANDARD-CLASS GCODE:GC-STATE>)
UNIT : MM
MOTOR : NIL
MOTION : NIL
MODE : NIL
MOC-OFFSET : NIL
WOC-POS : NIL
FEED : NIL
SPINDLE : NIL
")
    #+ccl (lu:assert-equal f-b-s
                           "#<GC-STATE {...}> (#<STANDARD-CLASS GC-STATE>)
UNIT : MM
MOTOR : NIL
MOTION : NIL
MODE : NIL
MOC-OFFSET : NIL
WOC-POS : NIL
FEED : NIL
SPINDLE : NIL
")))

(lu:define-test test-pre-g-cmds ()
  (let* ((staged-cmds nil)
         (pre-g-cmds (pre-g-cmds staged-cmds nil)))
    (lu:assert-nil (first pre-g-cmds))
    (lu:assert-nil (second pre-g-cmds)))
  (let* ((staged-cmds '("S10000" "M3" "G20" "G90"
                        "G1" "X3" "y-.02" "Z45" "F4" "M5"))
         (pre-g-cmds (pre-g-cmds staged-cmds nil)))
    (lu:assert-equal '("S10000" "M3" "G20" "G90") (first pre-g-cmds))
    (lu:assert-equal '("G1" "X3" "y-.02" "Z45" "F4" "M5") (second pre-g-cmds)))
  (let* ((staged-cmds '("G1" "X3" "y-.02" "Z45" "F4" "M5"))
         (pre-g-cmds (pre-g-cmds staged-cmds nil)))
    (lu:assert-nil (first pre-g-cmds))
    (lu:assert-equal '("G1" "X3" "y-.02" "Z45" "F4" "M5") (second pre-g-cmds))))

(lu:define-test test-post-g-cmds ()
  (let* ((staged-cmds nil)
         (post-g-cmds (post-g-cmds staged-cmds nil)))
    (lu:assert-nil (first post-g-cmds))
    (lu:assert-nil (second post-g-cmds)))
  (let* ((staged-cmds '("G1" "X3" "y-.02" "Z45" "F4" "M5"))
         (post-g-cmds (post-g-cmds staged-cmds nil)))
    (lu:assert-equal '("G1" "X3" "y-.02" "Z45" "F4") (first post-g-cmds))
    (lu:assert-equal '("M5") (second post-g-cmds))))

(lu:define-test test-stage-3 ()
  (let* ((gc-block "G00 Z5.0000") 
         (s2 (stage-2
              (stage-1
               (make-stage-acc gc-block))))
         (pre-g-cmds (pre-g-cmds (staged-main-cmds s2) nil))
         (post-g-cmds (post-g-cmds
                       (second pre-g-cmds) nil))
         (s3 (stage-3 s2))
         (fps-nil (make-instance 'float-params)))
    (lu:assert-equal '(nil ("G00" "Z5.0000")) pre-g-cmds)
    (lu:assert-equal '(("G00" "Z5.0000") nil)  post-g-cmds)
    (lu:assert-true (staged-cmds= (staged-main-cmds s3)
                               (make-staged-cmds '(:G00)
                                                 (make-instance 'float-params
                                                                :z 5))))
    (lu:assert-true (staged-cmds= (staged-pre-cmds s3)
                               (make-staged-cmds nil fps-nil)))
    (lu:assert-true (staged-cmds= (staged-post-cmds s3)
                               (make-staged-cmds nil fps-nil)))
    (lu:assert-equal gc-block (gc-block s3)))
  (let* ((gc-block "M3 S24000") 
         (s2 (stage-2
              (stage-1
               (make-stage-acc gc-block))))
         (pre-g-cmds (pre-g-cmds (staged-main-cmds s2) nil))
         (post-g-cmds (post-g-cmds
                       (second pre-g-cmds) nil))
         (s3 (stage-3 s2))
         (fps-nil (make-instance 'float-params)))
    (lu:assert-equal '(("M03" "S24000") nil) pre-g-cmds)
    (lu:assert-nil post-g-cmds)
    (lu:assert-true (staged-cmds= (staged-pre-cmds s3)
                               (make-staged-cmds '(:M03)
                                                 (make-instance 'float-params
                                                                :s 24000))))
    (lu:assert-true (staged-cmds= (staged-main-cmds s3)
                               (make-staged-cmds nil fps-nil)))
    (lu:assert-true (staged-cmds= (staged-post-cmds s3)
                               (make-staged-cmds nil fps-nil)))
    (lu:assert-equal gc-block (gc-block s3)))
  (let* ((gc-block "N123 S10000 M3 G20 G90 G1 X3 y-.02 Z45 F4 M5") 
         (s2 (stage-2
              (stage-1
               (make-stage-acc gc-block))))
         (pre-g-cmds (pre-g-cmds (staged-main-cmds s2) nil))
         (post-g-cmds (post-g-cmds
                       (second pre-g-cmds) nil))
         (s3 (stage-3 s2)))
    (lu:assert-equal '(("S10000" "M03" "G20" "G90")
                    ("G01" "X3" "Y-.02" "Z45" "F4" "M05"))
                  pre-g-cmds)
    (lu:assert-equal '(("G01" "X3" "Y-.02" "Z45" "F4")
                    ("M05"))
                  post-g-cmds)
    (lu:assert-true (staged-cmds= (staged-pre-cmds s3)
                               (make-staged-cmds '(:M03 :G20 :G90)
                                                 (make-instance 'float-params
                                                                :s 10000))))
    (lu:assert-true (staged-cmds=
                  (staged-main-cmds s3)
                  (make-staged-cmds '(:G01)
                                    (make-instance 'float-params
                                                   :x 3 :y -0.02 :z 45 :f 4))))
    (lu:assert-true (staged-cmds= (staged-post-cmds s3)
                               (make-staged-cmds
                                '(:M05) (make-instance 'float-params))))
    
    (lu:assert-equal gc-block (gc-block s3)))
  (let* ((gc-block "N123 G20 G90 G1 X3 y-.02Z45 F4 M5") 
         (s2 (stage-2
              (stage-1
               (make-stage-acc gc-block))))
         (pre-g-cmds (pre-g-cmds (staged-main-cmds s2) nil))
         (post-g-cmds (post-g-cmds
                       (second pre-g-cmds) nil)) 
         (s3 (stage-3 s2)))
    (lu:assert-equal '(("G20" "G90")
                    ("G01" "X3" "Y-.02" "Z45" "F4" "M05"))
                  pre-g-cmds)
            
    (lu:assert-equal '(("G01" "X3" "Y-.02" "Z45" "F4")
                    ("M05"))
                  post-g-cmds)
    (lu:assert-nil (pre-cmd s3))
    (lu:assert-true (staged-cmds= (staged-pre-cmds s3)
                               (make-staged-cmds
                                '(:G20 :G90) (make-instance 'float-params))))
    (lu:assert-true (staged-cmds= (staged-main-cmds s3)
                               (make-staged-cmds
                                '(:G01)
                                (make-instance 'float-params
                                               :x 3 :y -0.02 :z 45 :f 4))))
    (lu:assert-true (staged-cmds= (staged-post-cmds s3)
                               (make-staged-cmds
                                '(:M05) (make-instance 'float-params)))))
  (let* ((gc-block "N123 X4 S10000 M3 G20 G90 G1 X3 y-.02 Z45 F4 M5") 
         (s2 (stage-2
              (stage-1
               (make-stage-acc gc-block))))
         (pre-g-cmds (pre-g-cmds (staged-main-cmds s2) nil))
         (post-g-cmds (post-g-cmds
                       (second pre-g-cmds) nil))
         (st-pre-cmds (make-staged-cmds '(:M03 :G20 :G90)
                                        (make-instance 'float-params
                                                       :x 4
                                                       :s 10000)))
         (st-main-cmds (make-staged-cmds '(:G01)
                                         (make-instance 'float-params
                                                        :x 3 :y -0.02 :z 45
                                                        :f 4)))
         (st-post-cmds (make-staged-cmds '(:M05)
                                         (make-instance 'float-params)))
         (s3 (stage-3 s2)))
    (lu:assert-equal '(("X4" "S10000" "M03" "G20" "G90")
                    ("G01" "X3" "Y-.02" "Z45" "F4" "M05"))
                  pre-g-cmds)
    (lu:assert-equal '(("G01" "X3" "Y-.02" "Z45" "F4")
                    ("M05"))
                  post-g-cmds)
    (lu:assert-true (staged-cmds= (staged-pre-cmds s3) st-pre-cmds))
    (lu:assert-true (staged-cmds= (staged-main-cmds s3) st-main-cmds))
    (lu:assert-true (staged-cmds= (staged-post-cmds s3) st-post-cmds))
    (lu:assert-true (stage-acc= s3
                             (make-instance 'stage-acc
                                            :gc-block gc-block
                                            :gc-state-0 (make-instance 'gc-state)
                                            :staged-pre-cmds st-pre-cmds
                                            :staged-main-cmds st-main-cmds
                                            :staged-post-cmds st-post-cmds))))
  (let* ((gc-block "N123 G20 G90 G1 X3 y-.02Z45 F4 M3 M5") 
         (s2 (stage-2
              (stage-1
               (make-stage-acc gc-block))))         
         (pre-g-cmds (pre-g-cmds (staged-main-cmds s2) nil))
         (post-g-cmds (post-g-cmds
                       (second pre-g-cmds) nil))
         (s3 (stage-3 s2))
         (s3-s (gc-block s3)))
    (lu:assert-true (final s3))
    (lu:assert-true (equal 'gc-error (type-of (pre-cmd s3))))))

(lu:define-test test-modal-g-ok ()
  (lu:assert-true (modal-g-ok '(:G38.2 :G90)))
  (lu:assert-nil (modal-g-ok '(:G17 :G18 :G93))))

(lu:define-test test-modal-m-ok ()
  (lu:assert-true (modal-m-ok '(:M00 :M48)))
  (lu:assert-nil (modal-m-ok '(:M03 :M05)))
  (lu:assert-nil (modal-m-ok '(:M06 :M03 :M04 :M48))))

(lu:define-test test-is-unit? ()
  (let ((st (make-instance 'gc-state :unit :mm))
        (cmds nil))
    (lu:assert-nil (is-unit? nil nil))
    (lu:assert-eql :inch (is-unit? nil '(:G20)))
    (lu:assert-eql :mm (is-unit? st cmds))))

(lu:define-test test-is-mode? ()
  (let ((st (make-instance 'gc-state :mode :inc))
        (cmds nil))
    (lu:assert-nil (is-mode? nil nil))
    (lu:assert-eql :abs (is-mode? nil '(:G90)))
    (lu:assert-eql :inc (is-mode? st cmds))))

(lu:define-test linear-motion-fparams-p ()
  (lu:assert-nil (linear-motion-fparams-p nil))
  (lu:assert-nil (linear-motion-fparams-p
               (make-instance 'float-params :s 10000)))
  (lu:assert-nil (linear-motion-fparams-p
               (make-instance 'float-params :x 4 :k 0 :s 10000)))
  (lu:assert-true (linear-motion-fparams-p
                (make-instance 'float-params :x 4 :s 10000))))

(lu:define-test is-linear-motion? ()
  (let* ((fps-1 (make-instance 'float-params :s 10000))
         (fps-2 (make-instance 'float-params :x 4 :k 0 :s 10000))
         (fps-3 (make-instance 'float-params :x 4 :s 10000))
         (st-cmds-1 (make-staged-cmds nil nil))
         (st-cmds-2 (make-staged-cmds '(:ANY :G00) nil))
         (st-cmds-3 (make-staged-cmds '(:G00) fps-1))
         (st-cmds-4 (make-staged-cmds '(:G00) fps-2))
         (st-cmds-5 (make-staged-cmds '(:G00) fps-3))
         (st-cmds-6 (make-staged-cmds '(:G00 :G90) fps-3))
         (st-cmds-7 (make-staged-cmds '(:G00 :G20 :G90) fps-3))
         (st-1 (make-instance 'gc-state :unit :inch))
         (st-2 (make-instance 'gc-state :unit :mm :mode :inc)))
    (lu:assert-nil (is-linear-motion? nil nil))
    #+sbcl
    (lu:assert-error 'simple-error (is-linear-motion? '(:ANY :ST) st-cmds-5))
    #+ecl
    (lu:assert-error 'simple-type-error (is-linear-motion? '(:ANY :ST) st-cmds-5))
    #+sbcl
    (lu:assert-error 'simple-error (is-linear-motion? st-2 '(:ANY :FPS)))
    #+ecl
    (lu:assert-error 'simple-type-error (is-linear-motion? st-2 '(:ANY :FPS)))
    (lu:assert-nil (is-linear-motion? st-2 st-cmds-1))
    (lu:assert-nil (is-linear-motion? st-2 st-cmds-2))
    (lu:assert-nil (is-linear-motion? st-2 st-cmds-3))
    (lu:assert-nil (is-linear-motion? st-2 st-cmds-4))
    (lu:assert-nil (is-linear-motion? st-1 st-cmds-5))
    (lu:assert-equal '(:G00 :G90) (gc-list (is-linear-motion? st-1 st-cmds-6)))
    (lu:assert-true (string=
                  (format-by-slots
                   (f-params (is-linear-motion? st-1 st-cmds-6))
                   '(x y z i j k s f) "FLOAT-PARAMS")
                  #-ccl "#<FLOAT-PARAMS {...}> (#<STANDARD-CLASS GCODE::FLOAT-PARAMS>)
X : 4
Y : NIL
Z : NIL
I : NIL
J : NIL
K : NIL
S : 10000
F : NIL
"
                  #+ccl "#<FLOAT-PARAMS {...}> (#<STANDARD-CLASS FLOAT-PARAMS>)
X : 4
Y : NIL
Z : NIL
I : NIL
J : NIL
K : NIL
S : 10000
F : NIL
"))
    (lu:assert-nil (is-linear-motion? nil st-cmds-1))
    (lu:assert-nil (is-linear-motion? nil st-cmds-5))
    (lu:assert-nil (is-linear-motion? nil st-cmds-6))
    (lu:assert-equal '(:G00 :G20 :G90) (gc-list (is-linear-motion?  nil st-cmds-7)))))

(lu:define-test test-redundant-cmd-p ()
  (let* ((spindle (make-instance 'spindle-state))
         (state (make-instance 'gc-state :spindle spindle)))
    (setf (spindle state) spindle)
    (setf (flood spindle) :on)
    (setf (mist spindle) :on)
    (setf (unit state) :mm)
    (setf (mode state) :abs)
    (setf (motion state) :jog)
    (lu:assert-true (eql :on (flood
                           (spindle state))))
    (lu:assert-true (redundant-cmd-p state :M07))
    (lu:assert-true (redundant-cmd-p state :M08))
    (lu:assert-false (redundant-cmd-p state :M09))
    (lu:assert-true (redundant-cmd-p state :G00))
    (lu:assert-false (redundant-cmd-p state :G01))
    (lu:assert-false (redundant-cmd-p state :G02))
    (lu:assert-false (redundant-cmd-p state :G03))
    (lu:assert-true (redundant-cmd-p state :G21))
    (lu:assert-false (redundant-cmd-p state :G20))
    (lu:assert-true (redundant-cmd-p state :G90))
    (lu:assert-false (redundant-cmd-p state :G91))
    (lu:assert-true (funcall (drop-cmd-p state) :M07))
    (lu:assert-false (funcall (drop-cmd-p state) :G00)))
  (let* ((gc-block "N123 S10000 M3 G20 G90 G1 X3 y-.02 Z45 F4 M5") 
         (s3 (stage-3
              (stage-2
               (stage-1
                (make-stage-acc gc-block)))))
         (s3-gc-state-0 (gc-state-0 s3)))
    (lu:assert-nil (redundant-cmd-p s3-gc-state-0 :M03))
    (lu:assert-nil (redundant-cmd-p s3-gc-state-0 :G01))
    (lu:assert-nil (redundant-cmd-p s3-gc-state-0 :G20))
    (lu:assert-nil (redundant-cmd-p s3-gc-state-0 :G90))
    (lu:assert-equal 'spindle-state (type-of (spindle s3-gc-state-0))))
  (let* ((gc-block "N123 S10000 M3 G20 G90 G1 X3 y-.02 Z45 F4 M5") 
         (s4 (stage-4
              (stage-3
               (stage-2
                (stage-1
                 (make-stage-acc gc-block))))))
         (s4-gc-state-0 (gc-state-0 s4)))
    (lu:assert-nil (redundant-cmd-p s4-gc-state-0 :M03))
    (lu:assert-nil (redundant-cmd-p s4-gc-state-0 :G01))
    (lu:assert-true (redundant-cmd-p s4-gc-state-0 :G20))
    (lu:assert-true (redundant-cmd-p s4-gc-state-0 :G90))
    (lu:assert-equal 'spindle-state (type-of (spindle s4-gc-state-0))))
  (let* ((gc-block "N123 S10000 M3 G20 G90 G1 X3 y-.02 Z45 F4 M5")
         (gc-state-0 (make-instance 'gc-state :motion :lin))
         (s4 (stage-4
              (stage-3
               (stage-2
                (stage-1
                 (make-instance 'stage-acc
                                :gc-block gc-block
                                :gc-state-0 gc-state-0))))))
         (s4-gc-state-0 (gc-state-0 s4)))
    (lu:assert-nil (redundant-cmd-p s4-gc-state-0 :M03))
    (lu:assert-true (redundant-cmd-p s4-gc-state-0 :G01))
    (lu:assert-true (redundant-cmd-p s4-gc-state-0 :G20))
    (lu:assert-true (redundant-cmd-p s4-gc-state-0 :G90))
    (lu:assert-equal 'spindle-state (type-of (spindle s4-gc-state-0)))))

(lu:define-test test-drop-cmd-p ()
  (let* ((gc-block "N123 S10000 M3 G20 G90 G1 X3 y-.02 Z45 F4 M5") 
         (s3 (stage-3
              (stage-2
               (stage-1
                (make-stage-acc gc-block)))))
         (s3-gc-state-0 (gc-state-0 s3)))
    (lu:assert-nil (funcall (drop-cmd-p s3-gc-state-0) :M03))
    (lu:assert-nil (funcall (drop-cmd-p s3-gc-state-0) :G01))
    (lu:assert-nil (funcall (drop-cmd-p s3-gc-state-0) :G20))
    (lu:assert-nil (funcall (drop-cmd-p s3-gc-state-0) :G90))
    (lu:assert-equal 'spindle-state (type-of (spindle s3-gc-state-0))))
  (let* ((gc-block "N123 S10000 M3 G20 G90 G1 X3 y-.02 Z45 F4 M5") 
         (s4 (stage-4
              (stage-3
               (stage-2
                (stage-1
                 (make-stage-acc gc-block))))))
         (s4-gc-state-0 (gc-state-0 s4)))
    (lu:assert-nil (funcall (drop-cmd-p s4-gc-state-0) :M03))
    (lu:assert-nil (funcall (drop-cmd-p s4-gc-state-0) :G01))
    (lu:assert-true (funcall (drop-cmd-p s4-gc-state-0) :G20))
    (lu:assert-true (funcall (drop-cmd-p s4-gc-state-0) :G90))
    (lu:assert-equal 'spindle-state (type-of (spindle s4-gc-state-0)))))

(lu:define-test test-prune-cmds ()
  (let* ((gc-block "N123 S10000 M3 G20 G90 G1 X3 y-.02 Z45 F4 M5") 
         (s3 (stage-3
              (stage-2
               (stage-1
                (make-stage-acc gc-block)))))          
         (s3-gc-state-0 (gc-state-0 s3))
         (s3-staged-pre-cmds (staged-pre-cmds s3))
         (r (remove-if (drop-cmd-p s3-gc-state-0)
                       (cmds s3-staged-pre-cmds))))
    (lu:assert-equal '(:M03 :G20 :G90) r)
    (lu:assert-equal '(:M03 :G20 :G90 :G00) (append r '(:G00)))
    (lu:assert-equal '(:M03 :G20 :G90) (prune-cmds s3-gc-state-0
                                                s3-staged-pre-cmds))
    (lu:assert-equal 'spindle-state (type-of (spindle s3-gc-state-0))))
  (let* ((gc-block "N123 S10000 M3 G20 G90 G1 X3 y-.02 Z45 F4 M5") 
         (s4 (stage-4
              (stage-3
               (stage-2
                (stage-1
                 (make-stage-acc gc-block))))))          
         (s4-gc-state-0 (gc-state-0 s4))
         (s4-staged-main-cmds (staged-main-cmds s4))
         (r (remove-if (drop-cmd-p s4-gc-state-0) (cmds s4-staged-main-cmds))))
    (lu:assert-equal '(:G01) r)
    (lu:assert-equal '(:G01 :G00) (append r '(:G00)))
    (lu:assert-equal '(:G01) (prune-cmds s4-gc-state-0 s4-staged-main-cmds))
    (lu:assert-equal 'spindle-state (type-of (spindle s4-gc-state-0)))))

(lu:define-test test-update-woc-pos ()
  (let* ((st (make-instance 'gc-state))
         (fps (make-instance 'float-params))
         (woc-pos (update-woc-pos st fps)))
    (lu:assert-equal '(0 0 0)
                  (list (x (woc-pos st))
                        (y (woc-pos st))
                        (z (woc-pos st)))))
  (let* ((st (make-instance 'gc-state
                            :mode :inc
                            :woc-pos (make-instance 'point-3 :x -3)))
         (fps (make-instance 'float-params
                             :x 9 :z -2))
         (woc-pos (update-woc-pos st fps)))
    (lu:assert-equal '(6 nil -2)
                  (list (x (woc-pos st))
                        (y (woc-pos st))
                        (z (woc-pos st)))))
  (let* ((st (make-instance 'gc-state
                            :woc-pos (make-instance 'point-3 :x -3)))
         (fps (make-instance 'float-params
                             :x 9 :z -2))
         (woc-pos (update-woc-pos st fps)))
    (lu:assert-equal '(9 nil -2)
                  (list (x (woc-pos st))
                        (y (woc-pos st))
                        (z (woc-pos st))))))

(lu:define-test test-stage-4 ()
  (let* ((gc-block "G00 Z5.0000") 
         (s4 (stage-4
              (stage-3
               (stage-2
                (stage-1
                 (make-stage-acc gc-block))))))
         (gc-pre-state (make-instance 'gc-state)))
    (lu:assert-nil (cmds (staged-pre-cmds s4)))
    (lu:assert-true (float-params= (make-instance 'float-params)
                                (fps (staged-pre-cmds s4)))) 
    (lu:assert-equal '(:G00) (cmds (staged-main-cmds s4)))
    (lu:assert-true (float-params= (make-instance 'float-params :z 5)
                                (fps (staged-main-cmds s4))))
    (lu:assert-nil (cmds (staged-post-cmds s4)))
    (lu:assert-true (float-params= (make-instance 'float-params)
                                (fps (staged-post-cmds s4))))
    (lu:assert-nil (final s4))
    (lu:assert-nil (linear-motion-fparams-p (fps (staged-pre-cmds s4))))
    (lu:assert-equal 'gc-nil (type-of (pre-cmd s4)))
    (lu:assert-true (gc-state= gc-pre-state (gc-pre-state s4))))
  (let* ((gc-block "M3 S24000") 
         (s4 (stage-4
              (stage-3
               (stage-2
                (stage-1
                 (make-stage-acc gc-block))))))
         (gc-pre-state (make-instance 'gc-state
                                      :woc-pos (make-instance 'point-3
                                                              :x 0 :y 0 :z 0)
                                      :unit :mm
                                      :spindle (make-instance 'spindle-state))))
    (lu:assert-equal '(:M03) (cmds (staged-pre-cmds s4)))
    (lu:assert-true (float-params= (make-instance 'float-params :s 24000)
                                (fps (staged-pre-cmds s4)))) 
    (lu:assert-nil (cmds (staged-main-cmds s4)))
    (lu:assert-true (float-params= (make-instance 'float-params)
                                (fps (staged-main-cmds s4))))
    (lu:assert-nil (cmds (staged-post-cmds s4)))
    (lu:assert-true (float-params= (make-instance 'float-params)
                                (fps (staged-post-cmds s4))))
    (lu:assert-nil (final s4))
    (lu:assert-nil (linear-motion-fparams-p (fps (staged-pre-cmds s4))))
    (lu:assert-equal 'gc-multi-cmd (type-of (pre-cmd s4)))
    (lu:assert-true (gc-state= gc-pre-state (gc-pre-state s4))))
  (let* ((gc-block "M3 x -3 S24000") 
         (s4 (stage-4
              (stage-3
               (stage-2
                (stage-1
                 (make-stage-acc gc-block)))))))
    (lu:assert-equal '(:M03) (cmds (staged-pre-cmds s4)))
    (lu:assert-true (float-params= (make-instance 'float-params :x -3 :s 24000)
                                (fps (staged-pre-cmds s4)))) 
    (lu:assert-nil (cmds (staged-main-cmds s4)))
    (lu:assert-true (float-params= (make-instance 'float-params)
                                (fps (staged-main-cmds s4))))
    (lu:assert-nil (cmds (staged-post-cmds s4)))
    (lu:assert-true (float-params= (make-instance 'float-params)
                                (fps (staged-post-cmds s4))))
    (lu:assert-true (final s4))
    (lu:assert-true (linear-motion-fparams-p (fps (staged-pre-cmds s4))))
    (lu:assert-equal 'gc-error (type-of (pre-cmd s4))))
  (let* ((gc-block "M3 x -3 S24000")                 
         (gc-state-0 (make-instance 'gc-state        
                                    :unit :mm        
                                    :mode :abs))     
         (s4 (stage-4                                
              (stage-3                       
               (stage-2              
                (stage-1     
                 (make-instance 'stage-acc 
                                :gc-block gc-block 
                                :gc-state-0 gc-state-0))))))) 
    (lu:assert-equal '(:M03) (cmds (staged-pre-cmds s4))) 
    (lu:assert-true (float-params= (make-instance 'float-params :x -3 :s 24000) 
                                (fps (staged-pre-cmds s4))))         
    (lu:assert-nil (cmds (staged-main-cmds s4)))                        
    (lu:assert-true (float-params= (make-instance 'float-params)        
                                (fps (staged-main-cmds s4))))        
    (lu:assert-nil (cmds (staged-post-cmds s4)))                        
    (lu:assert-true (float-params= (make-instance 'float-params)        
                                (fps (staged-post-cmds s4))))        
    (lu:assert-true (final s4))                                         
    (lu:assert-true (linear-motion-fparams-p (fps (staged-pre-cmds s4)))) 
    (lu:assert-equal 'gc-error (type-of (pre-cmd s4)))) 
  (let* ((gc-block "M3 x -3 S24000")    
         (gc-state-0 (make-instance 'gc-state  
                                    :unit :mm                             
                                    :mode :abs       
                                    :motion :jog))                        
         (s4 (stage-4                          
              (stage-3                              
               (stage-2                              
                (stage-1                              
                 (make-instance 'stage-acc             
                                :gc-block gc-block                    
                                :gc-state-0 gc-state-0))))))
         (gc-pre-state (make-instance 'gc-state
                                      :woc-pos (make-instance 'point-3
                                                              :x -3 :y 0 :z 0)
                                      :unit :mm
                                      :mode :abs
                                      :motion :jog
                                      :spindle (make-instance 'spindle-state))))         
    (lu:assert-equal '(:M03 :G00) (cmds (staged-pre-cmds s4))) 
    (lu:assert-true (float-params= (make-instance 'float-params :x -3 :s 24000) 
                                (fps (staged-pre-cmds s4))))          
    (lu:assert-nil (cmds (staged-main-cmds s4))) 
    (lu:assert-true (float-params= (make-instance 'float-params) 
                                (fps (staged-main-cmds s4))))         
    (lu:assert-nil (cmds (staged-post-cmds s4))) 
    (lu:assert-true (float-params= (make-instance 'float-params) 
                                (fps (staged-post-cmds s4))))         
    (lu:assert-nil (final s4))               
    (lu:assert-true (linear-motion-fparams-p (fps (staged-pre-cmds s4)))) 
    (lu:assert-equal 'gc-motion (type-of (pre-cmd s4)))     
    (lu:assert-true (point-3= (make-instance 'point-3 :x -3 :y 0 :z 0) 
                           (woc-pos (gc-pre-state s4))))
    (lu:assert-true (gc-state= gc-pre-state (gc-pre-state s4))))
  (let* ((gc-block "N123 S10000 M3 G20 G90 G1 X3 y-.02 Z45 F4 M5") 
         (s4 (stage-4                          
              (stage-3                              
               (stage-2                              
                (stage-1                              
                 (make-stage-acc gc-block))))))
         (gc-pre-state (make-instance 'gc-state
                                      :woc-pos
                                      (make-instance 'point-3
                                                     :x 0 :y 0 :z 0)
                                      :unit :inch
                                      :mode :abs
                                      :spindle (make-instance 'spindle-state))))
    (lu:assert-equal '(:M03 :G20 :G90) (cmds (staged-pre-cmds s4)))
    (lu:assert-true (string= (format-float-params (fps (staged-pre-cmds s4))) 
                             #-ccl "#<FLOAT-PARAMS {...}> (#<STANDARD-CLASS GCODE::FLOAT-PARAMS>)
X : NIL
Y : NIL
Z : NIL
I : NIL
J : NIL
K : NIL
R : NIL
S : 10000
F : NIL
"
                             #+ccl "#<FLOAT-PARAMS {...}> (#<STANDARD-CLASS FLOAT-PARAMS>)
X : NIL
Y : NIL
Z : NIL
I : NIL
J : NIL
K : NIL
R : NIL
S : 10000
F : NIL
"))                                   
    (lu:assert-equal '(:G01) (cmds (staged-main-cmds s4))) 
    (lu:assert-true (float-params= (make-instance 'float-params :s 10000) 
                                (fps (staged-pre-cmds s4))))          
    (lu:assert-equal '(:M05) (cmds (staged-post-cmds s4))) 
    (lu:assert-true (float-params= (make-instance 'float-params) 
                                (fps (staged-post-cmds s4))))         
    (lu:assert-nil (final s4))               
    (lu:assert-nil (linear-motion-fparams-p (fps (staged-pre-cmds s4)))) 
    (lu:assert-equal 'gc-multi-cmd (type-of (pre-cmd s4))) 
    (lu:assert-equal '(:M03 :G20 :G90) (gc-list (pre-cmd s4)))
    (lu:assert-true (gc-cmd= (pre-cmd s4)
                          (make-instance 'gc-multi-cmd
                                         :gc-list '(:M03 :G20 :G90)
                                         :f-params (make-instance
                                                    'float-params :s 10000))))
    (lu:assert-true (gc-state= gc-pre-state (gc-pre-state s4))))  
  (let* ((gc-block "N123 x4 S10000 M3 G20 G90 G1 X3 y-.02 Z45 F4 M5") 
         (s4 (stage-4                          
              (stage-3                              
               (stage-2                              
                (stage-1                              
                 (make-stage-acc gc-block)))))))       
    (lu:assert-equal '(:M03 :G20 :G90) (cmds (staged-pre-cmds s4))) 
    (lu:assert-true (float-params= (make-instance 'float-params :x 4 :s 10000) 
                                (fps (staged-pre-cmds s4))))          
    (lu:assert-equal '(:G01) (cmds (staged-main-cmds s4))) 
    (lu:assert-true (float-params= (make-instance 'float-params 
                                               :x 3 :y -.02 :z 45 :f 4)              
                                (fps (staged-main-cmds s4))))         
    (lu:assert-equal '(:M05) (cmds (staged-post-cmds s4))) 
    (lu:assert-true (float-params= (make-instance 'float-params) 
                                (fps (staged-post-cmds s4))))         
    (lu:assert-true (final s4))              
    (lu:assert-true (linear-motion-fparams-p (fps (staged-pre-cmds s4))))
    (let ((st-pre-cmds (staged-pre-cmds s4))) 
      (lu:assert-true (and (linear-motion-fparams-p (fps st-pre-cmds)) 
                        (make-gc-error                        
                         "incomplete linear motion pre-command"))) 
      (lu:assert-equal 'gc-error               
                    (type-of (or (is-linear-motion? (gc-state-0 s4) st-pre-cmds) 
                                 (and (linear-motion-fparams-p (fps st-pre-cmds)) 
                                      (make-gc-error                        
                                       "incomplete linear motion pre-command")) 
                                 (make-gc-multi-cmd (cmds st-pre-cmds))))))                                       
    (lu:assert-equal 'gc-error (type-of (pre-cmd s4)))) 
  (let* ((gc-block "N123 X4 S10000 M3 G20 G90 G1 X3 y-.02 Z45 F4 M5") 
         (init-gc-state-0 (make-instance 'gc-state :unit :mm 
                                                   :motion :jog                          
                                                   :mode :abs))                          
         (s4 (stage-4                          
              (stage-3                              
               (stage-2                              
                (stage-1                              
                 (make-instance 'stage-acc             
                                :gc-block gc-block                    
                                :gc-state-0 init-gc-state-0))))))
         (gc-pre-state (make-instance 'gc-state
                                      :woc-pos
                                      (make-instance 'point-3
                                                     :x 4 :y 0 :z 0)
                                      :unit :inch
                                      :mode :abs
                                      :motion :jog
                                      :spindle (make-instance 'spindle-state))))
    (lu:assert-eql :inch (unit (gc-pre-state s4))) 
    (lu:assert-eql :abs (mode (gc-pre-state s4))) 
    (lu:assert-equal '(:M03 :G20 :G00) (cmds (staged-pre-cmds s4))) 
    (lu:assert-equal '(:M05) (cmds (staged-post-cmds s4))) 
    (lu:assert-true (float-params= (make-instance 'float-params :x 4 :s 10000) 
                                (fps (staged-pre-cmds s4))))          
    (lu:assert-equal gc-block (gc-block s4)) 
    (lu:assert-nil (final s4))               
    (lu:assert-true (float-params= (make-instance 'float-params :x 4 :s 10000) 
                                (f-params (pre-cmd s4))))
    (lu:assert-true (gc-state= gc-pre-state (gc-pre-state s4))))
  (let* ((gc-block "N123 X4 S10000 M3 G20 G91 G1 X3 y-.02 Z45 F4 M5") 
         (init-gc-state-0 (make-instance 'gc-state :unit :mm 
                                                   :motion :jog                          
                                                   :mode :abs))                          
         (s4 (stage-4                          
              (stage-3                              
               (stage-2                              
                (stage-1                              
                 (make-instance 'stage-acc             
                                :gc-block gc-block                    
                                :gc-state-0 init-gc-state-0))))))
         (gc-pre-state (make-instance 'gc-state
                                      :woc-pos
                                      (make-instance 'point-3
                                                     :x 4 :y 0 :z 0)
                                      :unit :inch
                                      :mode :inc
                                      :motion :jog
                                      :spindle (make-instance 'spindle-state))))  
    (lu:assert-eql :inch (unit (gc-pre-state s4))) 
    (lu:assert-eql :inc (mode (gc-pre-state s4))) 
    (lu:assert-equal '(:M03 :G20 :G91 :G00) (cmds (staged-pre-cmds s4))) 
    (lu:assert-equal '(:M05) (cmds (staged-post-cmds s4))) 
    (lu:assert-true (float-params= (make-instance 'float-params :x 4 :s 10000) 
                                (fps (staged-pre-cmds s4))))          
    (lu:assert-equal gc-block (gc-block s4)) 
    (lu:assert-nil (final s4))               
    (lu:assert-true (float-params= (make-instance 'float-params :x 4 :s 10000) 
                                (f-params (pre-cmd s4))))
    (lu:assert-true (gc-state= gc-pre-state (gc-pre-state s4)))))

(lu:define-test test-stage-5 ()  
  (let* ((gc-block "G00 Z5.0000")
         (gc-state-0 (make-instance 'gc-state
                                    :woc-pos (make-instance
                                              'point-3 :x 0 :y 0 :z 0)
                                    :unit :mm
                                    :mode :abs
                                    :motion nil
                                    :spindle nil))
         (s5 (stage-5
              (stage-4
               (stage-3
                (stage-2
                 (stage-1
                  (make-instance 'stage-acc :gc-block gc-block
                                            :gc-state-0 gc-state-0)))))))
         (fps (make-instance 'float-params :z 5))
         (s5-main-cmd (make-gc-motion '(:G00) fps))
         (gc-main-state (make-instance 'gc-state
                                       :woc-pos (make-instance
                                                 'point-3 :x 0 :y 0 :z 5)
                                       :unit :mm
                                       :mode :abs
                                       :motion :jog
                                       :spindle (make-instance
                                                 'spindle-state))))
    (lu:assert-true (is-linear-motion? (gc-pre-state s5) (staged-main-cmds s5)))
    (lu:assert-true (float-params= (f-params (main-cmd s5))
                                (f-params s5-main-cmd)))
    (lu:assert-true (equal (gc-list (main-cmd s5))
                        (gc-list s5-main-cmd)))
    (lu:assert-nil (cmds (staged-post-cmds s5)))
    (lu:assert-true (gc-cmd= (main-cmd s5) s5-main-cmd))
    (lu:assert-true (gc-state= gc-main-state (gc-main-state s5)))
    (lu:assert-nil (final s5)))
  (let* ((gc-block "N123 S10000 M3 G20 G90 G1 X3 y-.02 Z45 F4 M5") 
         (s5 (stage-5
              (stage-4
               (stage-3
                (stage-2
                 (stage-1
                  (make-stage-acc gc-block)))))))
         (fps (make-instance 'float-params :x (* 25.4 3)
                                           :y (* 25.4 -.02) 
                                           :z (* 25.4 45)
                                           :f 4))
         (s5-main-cmd (make-gc-motion '(:G01) fps))
         (gc-main-state (make-instance 'gc-state
                                       :woc-pos
                                       (make-instance 'point-3
                                                      :x (* 25.4 3)
                                                      :y (* 25.4 -.02) 
                                                      :z (* 25.4 45))
                                       :unit :inch
                                       :mode :abs
                                       :motion :lin
                                       :spindle (make-instance 'spindle-state))))
    (lu:assert-true (is-linear-motion? (gc-pre-state s5) (staged-main-cmds s5)))
    (lu:assert-true (float-params= (f-params (main-cmd s5))
                                (f-params s5-main-cmd)))
    (lu:assert-true (equal (gc-list (main-cmd s5))
                        (gc-list s5-main-cmd)))
    (lu:assert-equal (cmds (staged-post-cmds s5)) '(:M05))
    (lu:assert-true (gc-cmd= (main-cmd s5) s5-main-cmd))
    (lu:assert-true (gc-state= gc-main-state (gc-main-state s5)))
    (lu:assert-nil (final s5)))
  (let* ((gc-block "N123 S10000 M3 G20 G90 G2 X3 y-.02 Z45 i0 j-.9 F4 M5") 
         (s5 (stage-5
              (stage-4
               (stage-3
                (stage-2
                 (stage-1
                  (make-stage-acc gc-block)))))))
         (fps (make-instance 'float-params :x (* 25.4 3)
                                           :y (* 25.4 -.02)
                                           :z (* 25.4 45)
                                           :i 0
                                           :j (* 25.4 -0.9)
                                           :f 4))
         (s5-main-cmd (make-gc-helix '(:G17 :G02) fps))
         (gc-main-state (make-instance 'gc-state
                                       :woc-pos
                                       (make-instance 'point-3
                                                      :x (* 25.4 3)
                                                      :y (* 25.4 -.02) 
                                                      :z (* 25.4 45))
                                       :unit :inch
                                       :mode :abs
                                       :motion :cw
                                       :spindle (make-instance 'spindle-state))))
    (lu:assert-true (is-xy-helix? (staged-main-cmds s5)))
    (lu:assert-true (float-params= (f-params (main-cmd s5))
                                (f-params s5-main-cmd)))
    (lu:assert-true (equal (gc-list (main-cmd s5))
                        (gc-list s5-main-cmd)))
    (lu:assert-true (gc-cmd= (main-cmd s5) s5-main-cmd))
    (lu:assert-true (gc-state= gc-main-state (gc-main-state s5)))  
    (lu:assert-nil (final s5))))


(lu:define-test test-stage-6 ()
  (let* ((gc-block "G00 Z5.0000")
         (gc-state-0 (make-instance 'gc-state
                                    :woc-pos (make-instance
                                              'point-3 :x 0 :y 0 :z 0)
                                    :unit :mm
                                    :mode :abs
                                    :motion nil
                                    :spindle nil))
         (s6 (stage-6
              (stage-5
               (stage-4
                (stage-3
                 (stage-2
                  (stage-1
                   (make-instance 'stage-acc :gc-block gc-block
                                             :gc-state-0 gc-state-0))))))))
         (fps (make-instance 'float-params :z 5))
         (s6-main-cmd (make-gc-motion '(:G00) fps))
         (gc-post-state (make-instance 'gc-state
                                       :woc-pos (make-instance
                                                 'point-3 :x 0 :y 0 :z 5)
                                       :unit :mm
                                       :mode :abs
                                       :motion :jog
                                       :spindle (make-instance
                                                 'spindle-state))))
    (lu:assert-true (final s6))
    (lu:assert-true (gc-cmd= (main-cmd s6) s6-main-cmd))
    (lu:assert-true (gc-cmd= (pre-cmd s6) (make-gcode :gc-nil)))
    (lu:assert-true (gc-cmd= (post-cmd s6) (make-gcode :gc-nil)))
    (lu:assert-true (gc-state= (gc-post-state s6) gc-post-state)))
  (let* ((gc-block "N123 S10000 M3 G20 G90 G1 X3 y-.02 Z45 F4 M5") 
         (s6 (stage-6
              (stage-5
               (stage-4
                (stage-3
                 (stage-2
                  (stage-1
                   (make-stage-acc gc-block))))))))
         (fps (make-instance 'float-params :x (* 25.4 3)
                                           :y (* 25.4 -.02) 
                                           :z (* 25.4 45)
                                           :f 4))
         
         (s6-main-cmd (make-gc-motion '(:G01) fps))
         (s6-post-cmd (make-gc-multi-cmd '(:M05)
                                         (make-instance 'float-params))))
    (lu:assert-true (final s6))
    (lu:assert-true (gc-cmd= (pre-cmd s6)
                          (make-instance 'gc-multi-cmd
                                         :gc-list '(:M03 :G20 :G90)
                                         :f-params (make-instance
                                                    'float-params :s 10000))))
    (lu:assert-true (gc-cmd= (main-cmd s6) s6-main-cmd))

    (lu:assert-true (float-params= (f-params (post-cmd s6))
                                (f-params s6-post-cmd)))

    (lu:assert-equal (gc-list s6-post-cmd) (gc-list (post-cmd s6)))
    (lu:assert-true (gc-cmd= (post-cmd s6) s6-post-cmd)))
    
#|

  (print (cmds (staged-post-cmds s6)))

  (print (gc-list (post-cmd s6)))
  (print (gc-list s6-post-cmd))

  (print (format-float-params (f-params (post-cmd s6))))
  (print (format-float-params (f-params s6-post-cmd)))


  (let* ((gc-block "N123 S10000 M3 G20 G90 G2 X3 y-.02 Z45 i0 j-.9 F4 M5") 
         (s6 (stage-6
              (stage-5
               (stage-4
                (stage-3
                 (stage-2
                  (stage-1
                   (make-stage-acc gc-block))))))))
         (s6-post-cmd (make-gc-multi-cmd '(:M05)
                                         (make-instance 'float-params))))
    (lu:assert-true (final s6))
    (lu:assert-equal ;; does not expand FLOAT-PARAMS
     (format-replace (format-by-slots (post-cmd s6)
                                      gc-cmd-motion-slots
                                      "GC-MULTI-CMD")
                     "FLOAT-PARAMS")
     (format-replace (format-by-slots s6-post-cmd
                                      gc-cmd-motion-slots
                                      "GC-MULTI-CMD")
                     "FLOAT-PARAMS"))
    (lu:assert-true (gc-cmd= (post-cmd s6) s6-post-cmd))))
    |#
  
  )

  
(lu:print-errors (lu:run-tests :all))

